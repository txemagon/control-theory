# Linear Quadratic Gaussian (LQG)

![Linear Quadratic Gaussian](./images/lqg.png)

$$ \varepsilon = x - \hat{x} $$

$$ \dot{x} = Ax -BK_r\hat{x}+w_d $$

$$ \hat{x} = x - (x - \hat{x}) $$

 From these two:
$$ \dot{x} = Ax -BK_rx + BK_r(x-\hat{x}) + w_d $$
$$ \dot{\varepsilon} = (A -K_fC)\varepsilon + w_d - K_f w_n $$

We arrive to

```math
\frac{d}{dt} \begin{bmatrix}
x \\ 
\varepsilon

\end{bmatrix}
=
\begin{bmatrix}
A-BK_r & BK_r \\ 
0 & A-K_fC
\end{bmatrix}
\begin{bmatrix}
x \\
\varepsilon 
\end{bmatrix}
+
\begin{bmatrix}
I & 0 \\ 
I & -K_f
\end{bmatrix}
\begin{bmatrix}
w_d \\ 
w_n
\end{bmatrix}

```

We see by the _separation principle_ that the eigenvalues are preserved.
