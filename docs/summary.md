# Control Theory

## System Representations

% Transfer Function - State Space - - Zero Pole Gain - System Variables

```matlab

% To Generate TF
s = tf('s');
tf(num,den)
H = ss(A,B,C,D)
K = zpk(z,p,k)

% To reap from a TF
[A,B,C,D] = ssdata(G)
[z,p,k]=zpkdata(H,'v')
[num,den]=tfdata(K,'v')

% To form from its components
[num, den] = ss2tf(A,B,C,D)
[num,den]=ss2tf(A,B,C,D)
[A,B,C,D]=tf2ss(num,den)
[z,p,k]=tf2zp(num,den)
[z,p,k]=ss2zp(A,B,C,D)
[A,B,C,D] = zp2ss(z,p,k)
[num,den] = zp2tf(z,p,k)

```

## System Identification

### Order

Step response:

1. Zero slope response to step 2 degrees higher.
1. Damped oscillation, 2nd order.

Bode plot:

1. Phase dropping below 90º, 2nd Order 

### Step Response

Damping Ratio: $$ \zeta = \frac{-\ln(M_p)}{ \sqrt{\pi^2 + \ln^2{M_p}} } $$  
Natural Frequency: $$ \omega_n = \frac{\omega_d}{\sqrt{1-\zeta^2}} $$  
Damped frequency: $$ \omega_d=\frac{2\pi}{\Delta t} $$  

### Bode Plot

DC Gain: $$ K = 10^{\frac{M(0)}{20}} $$  
Damping Ratio: $$ \zeta = \frac{K}{2 \cdot 10^{M (-90º) / 20}} $$  

## Equations

First Order: $$ G(s) = \frac{b}{(s+a)} = \frac{K}{\tau s + 1} $$  
Second Order: $$ G(s) = \frac{a}{s^2+bs+c} = \frac{K\omega_n^2}{s^2+2\zeta\omega_n+\omega_n^2} $$  


## System Analysis

```matlab
tf('s')
[A,B,C,D] = ssdata(G)
pole(G)
eig(A)
step(u*G)
pzmap(G2)
bode(G1, G2, G3)
linearSystemAnalizer('step', G)
```

### First Order Systems

Transfer function: $$ G(s) = \frac{b}{s + a} = \frac{k_{dc}}{\tau s + 1} $$  
DC Gain: $$ k_{dc} = \frac{b}{a} $$  
Time constant (time to reach 63% of steady state value): $$ \tau = \frac{1}{a} $$ 
Poles at -a.  

Seting Time:

- 10% => Ts = 2.3/a = 2.3 Tc
-  5% => Ts = 3.0 Tc
-  2% => Ts = 3.9 Tc
-  1% => Ts = 4.6 Tc


Rise Time:

- Time to rise from 10% to 90%

### Second-order Systems

$$ G(s) = \frac{1}{m s^2 + b s + k} = \frac{ k_{dc} \omega_n^2}{ s^2 + 2 \zeta \omega_n s + \omega_n^2} $$

- DC Gain: 1/k  
- Damping ratio: $$ \zeta = \frac{b}{2 \sqrt{ k \cdot m }} $$
- Natural frequency: $$ \omega_n = \sqrt{ \frac{k}{m} } $$

Poles / zeros: 
- The canonical 2nd order function has two poles at: $$ s_p = - \zeta \omega_n \plusminus j \omega_n \sqrt{1-\zeta^2} $$

- Underdamped systems: $$ \zeta < 1 $$
- $$ \omega_d = \omega_n \sqrt{1-\zeta^2} $$

- Setting time: $$ T_s = \frac{- \ln(tolerance fraction) }{\zeta\omega_n} $$
    - 10% => Ts = 2.3/a = 2.3 Tc
    -  5% => Ts = 3.0 / (zeta*w_n)
    -  2% => Ts = 3.9 / (zeta*w_n)
    -  1% => Ts = 4.6 / (zeta*w_n)
    
- Percent Overshoot: $$ M_p = \exp{ \frac{-\zeta \pi}{\sqrt{1 - \zeta^2}} } $$
- 


Settling time:            $$ T_s =  \frac{4.6}{\zeta \omega_n} $$  
Rising time 10-90%:	$$ T_r = \frac{1.8}{\omega_n} $$  
Damping Ratio: 	$$ \zeta = \frac{-ln(Mp)}{\sqrt{ \pi^2 + ln^2(Mp) }} $$  
Quality factor: $$ Q = \frac{1}{2 \zeta} $$  



```matlab
pole(G)
step(u*G)
linearSystemaAnalizer('step', G)
bode(G)
pzmap(G1)
C = pid(kp, ki, kd)
C = tf(C)
F = feedback(C*P,1)
pidtune
pidTuner(P,C)
opts = pidtuneOptions('CrossoverFrequency', 32, 'PhaseMargin', 90)
[C, info] = pidtune(P, 'pid', opts)

rlocus(sys)
sgrid(zeta, wn)
[K, poles] = rlocfind(sys)
controlSystemDesigner(plant)
```

## Frequency Domain Methods

- Phase Margin:
- Gain Margin:
- Crossover Frequency:
- Bandwidth Frequency:
    - Crossing below 3dB on closed loop
    - Crossing below [6 - 7.5 db] with PMargin [-135º - 225º] in open loop.
    

- Cauchy criterion and Nyquist diagram: $$ Z = N + P $$  

```matlab
roots([1 -8 15])
margin(100*G)
lsim(G, u, t)
nyquist(G)
lnyquist(G)

```

Bandwidth: $$ \omega_{bw} = \frac{t_s \zeta}{4 \pi} \sqrt{1 - 2\zeta^2 + \sqrt{4\zeta^4 - 2 \zeta^2 + 2}} $$  

## State Space Methods

```math
\begin{aligned}
\mathbf{\dot{x}} &= \mathbf{A x} + \mathbf{B u}  \\
\mathbf{y} &= \mathbf{Cx} + \mathbf{Du}
\end{aligned}

```

Aside from the direct contribution of of the input into the ouput, we can draw

![State-Space Diagram](./images/open_loop_plant.png)

$$ \dot_x = (A-BK)x $$

and 

$$ G(s) = C (sI-A)^{-1}B + D $$

- eig(A): poles of tf

### Controllability

If the system is controllable:

$$ Con = [ A BA B^2A \ldots B^{n-1}A  ] $$

```matlab
rank(ctrb(A, B) % shall be full ranked.
```

The poles of
$$ (A -BK) $$

can placed at desired positions, p.


```matlab
t = 0:0.01:2;
u = zeros(size(t));
x0 = [0.01 0 0];

p1 = -10 + 10i;
p2 = -10 - 10i;
p3 = -50;


K = place(A,B,[p1 p2 p3]);
sys_cl = ss(A-B*K,B,C,0);
lsim(sys_cl,u,t,x0);
xlabel('Time (sec)')
ylabel('Ball Position (m)')

```

To optimize pole placement using Linear Quadratic Regression see the cart pendulum example.

Te response to 

$$ \mathbf{\dot{x}} = \mathbf{A x} + \mathbf{B u} $$ 

is on  the form of

$$ x(t) = e^{At}x(0) + \int_0^t e^{A(t-\tau)}Bu(\tau)d\tau $$


#### Controllable?

PBH test (Popov-Belevitch-Hautus)

$$ rank[(A - \lambda I) B] = n $$

1. rank of () = n except for eigenvalues
1. B needs components in each eigenvector connection.
1. If B random the system will be controllable with high probability.

#### Degrees of controllability

The Gramian is

$$ W_t = \int_0^t e^{A\tau}BB^Te^{A^T\tau}d\tau \in \mathbb{R}^n $$

Aproximately:

$$ W_t = \mathcal{C}\mathcal{C}^T $$

In matlab through singular value decomposition:

```matlab
[U,S,V] = svd( ctrb(A,B), 'econ');
```

### Observability

A system is observable if:

```math
Obs = \begin{bmatrix}
C      \\
CA     \\
CA^2   \\
\vdots \\
CA^{n-1}

\end{bmatrix}

```

And the state, x, can be observed.

If the state cannot be observed, then it can be simulated with a faster model.

![Observable Plant](./images/observable_plant.png)

The equations for the model:

$$ \dot{\hat{x}} = A\hat{x} + Bu + L(y-\hat{y}) $$  
$$ \hat{y} = C\hat{x} $$  


Error dynamics:

$$ \dot{e} = \dot{x} - \dot{\hat{x}} = (A-LC)e $$  


The most observable variables can be calculated:

```matlab
[u,s,v] = svd( obsv(A,C) )  % Singular value decomposition
V` % Most observable states
```

### Estimators: Kalman Filter

$$ y = Ax + Bu + w_d $$ 
$$ y = Cx + w_n $$

Assuming Gaussian distributions for the disturbance and the sensor noise,
and knowing their variances:

It can be optimized J, the expectation of error:

$$ J = E[ (x - x^T)(x - x^T) $$

using linear quadratic estimator:

```matlab
Kf = lqe(A,C,Vd, Vn);
```

### Notes

For in depth knowledge look at

1. PBH Test
1. Gramian
1. Cayley-Hamilton Theorem

## Digital Control

### Fundamentals

Our analog model:

![Analog Controller](./images/continous_controller.png)

But when we arrive to digital control we get something like this:

![Digital Controller](./images/digital_controller.png)

To build a digital of model to work with we create the zero order hold function.

![Zero Order Hole Function](./images/zero_hold_equivalence.png)

And we arrive to a digital model like this:

![Digital Model of the Plant](./images/digital_image.png)


### Commands

A conversion example:

```matlab

% A conversion example:

m = 1; b = 10; k = 20;

s = tf('s');
sys = 1/(m*s^2+b*s+k);

Ts = 1/100;
sys_d = c2d(sys,Ts,'zoh')

% A digital from the begining system:
numDz = 1;
denDz = [1 -0.3 0.5];
sys = tf(numDz, denDz, -1); % the -1 indicates that the sample time is undetermined

pzmap(sys)
axis([-1 1 -1 1])
zgrid

sys = tf(numDz,denDz,1/20);
step(sys,2.5);
```

### Discrete Root Locus

$$ 1 + KG(z)H_{zoh}(z) = 0 $$

```matlab
numDz = [1 -0.3];
denDz = [1 -1.6 0.7];
sys = tf(numDz,denDz,-1);

rlocus(sys)
axis([-1 1 -1 1])

zeta = 0.4;
Wn = 0.3;
zgrid(zeta,Wn)
```




