# Robust Control

## Sensitivity & Complementary Sensitivity

![Full Model with Disturbance and Noise](./images/noise.png)

$$ y = P_dd + PK\varepsilon $$
$$ y = P_dd + PK(r-y-n) $$
$$ (I+PK)y = PKr + P_dd - PKn $$
$$ y = (I+PK)^{-1}PKr + (I+PK)^{-1}P_dd - (I+PK)^{-1}PKn $$
$$ S=(I+L)^{-1} $$
$$ T=(I+L)^{-1}L $$
$$ S + T = I $$

- L: Loop transfer function (PK)
- S: Sensitivity
- T: Complementary Sensitivity

Error:  

$$ \varepsilon = r -y_m = Sr - SP_dd  + Tn $$

Goal: Find the crossover angular velocity or design S for a given crossover.

## Loop Sensitivity


| w | Goal | L  |
|---|------|----|
| ↓ | S↓   | L↑ |
| ↑ | T↓   | L↓ |

L has to be a decreasing ramp in Bode Plot.

$$ L = \frac{\omega_B}{s} $$


## Considerations

- Nyquist stability criterion. Distance to the -1.
- Right Hand Plane Zeros and delays are margins.
- If the model is not linear -> Model Predictive Control.
