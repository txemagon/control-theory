%% Controlling DC motor speed

clear all

% Requirements:
% sse < 1%
% Ts < 2s
% Mp < 5%


%% Physical Characteristics

J = 0.01;
b = 0.1;
K = 0.01;
R = 1;
L = 0.5;

%% Transfer function

s = tf('s');
P_motor = K/((J*s+b)*(L*s+R)+K^2);

%% State Space

A = [
    -b/J    K/J  ;
    -K/L    -R/L
    ];

B = [
    0  ;
    1/L
    ];

C = [1 0];
D = 0;

motos_ss = ss(A,B,C,D);

% Alternatively
% motor_ss = ss(P_motor);

%% Analysis

linearSystemAnalyzer('step', P_motor, 0:0.1:5);

% There are two poles: -2, and -10,
% The system must be similar to
% rP_motor = 0.1/(0.5*s+1):