%% DC Motor Speed PID Design

clear all

% Requirements:
% Ts < 2s
% Mp < 5%
% sse < 1%


%% Phisical Model

% Data
J = 0.01;
b = 0.1;
K = 0.01;
R = 1;
L = 0.5;

% Plant

s = tf('s');
P_motor = K/((J*s+b)*(L*s+R)+K^2)

%% Proportional Control

% Definition trial
Kp = 100;
C = pid(Kp);
sys_cl = feedback(C*P_motor,1);

% Step Response
t = 0:0.01:5;
step(sys_cl,t)
grid
title('Step Response with Proportional Control')