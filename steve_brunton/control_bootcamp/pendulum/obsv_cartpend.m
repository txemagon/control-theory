clear all, close all, clc

%% Characterization
m = 1;
M = 5;
L = 2;
g = -10;
d = 1;

s = 1; % Sign. Indicates pendulum up.


%% Model
% y = [ x x' theta theta']
A = [ 
    0       1           0               0;
    0       -d/M      -m*g/M            0;
    0       0           0               1;
    0   -s*d/(M*L) -s*(m+M)*g/(M*L)     0
    ];

B = [0; 1/M; 0; s*1/(M*L)];

C = [1 0 0 0];

obsv(A,C)
det(obsv(A,c))

% We observe the x, but the other 3 variables are the one that we want to
% stabilize

%%
%% Which measurements are best if we omit "x"

A = A(2:end, 2:end):
B = B(2:end)
C = [1 0 0]; % measure xdot
% C = [0 1 0];
% C = [0 0 1];
obsv(A,C)

D = zeros(size(C,1), size(B,2));
sys = ss(a,B,C,D);
det(gram(sys,'o')) % Gramian (execute with s = -1)

%% Control

% Measures
eig(A);
rank(ctrb(A,B));

Q = [
    1   0   0   0;
    0   1   0   0;
    0   0   10  0;
    0   0   0   100;
    ];      % Cost for not reaching the targets

R = .001;     % Cost of controlling

% Pole Placement

% p = [-.01; -.02; -.03; -.04];
% p = [-.1; -.2; -.3; -.4];
% p = [ -.5; -.6; -.7; -.8];   % DC Error
% p = [ -.5; -.7; -.9; -1.1];  % Still DC Error
p = [-1; -1.1; -1.2; -1.3];
% p = [-2; -2.1; -2.2; -2.3];
% p = [-3; -3.1; -3.2; -3.3];
% p = [-3.5; -3.6; -3.7; -3.8];


% K = place(A,B,p);
K = lqr(A,B,Q,R);

tspan = 0:.001:10;

if (s==-1)
    y0 = [0; 0; 0; 0];
    [t,y] = ode45(@(t,y)pendcart(y,m,M,L,g,d,-K*(y-[4; 0; 0; 0])), tspan, y0);
elseif(s==1)
    y0 = [-3; 0; pi+.1;0];
    [t,y] = ode45(@(t,y)pendcart(y,m,M,L,g,d,-K*(y-[1; 0; pi; 0])), tspan, y0);
else
end


for k=1:100:length(t)
    drawpend(y(k,:), m, M, L);
end
