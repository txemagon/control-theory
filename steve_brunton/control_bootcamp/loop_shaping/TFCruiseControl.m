clear all, close all, clc

% Create a car model

sysCar = ss(-1, 1, 1, 0);

% [num, den] = tfdata(sysCar);
[num, den] = ss2tf(sysCar.a, sysCar.B, sysCar.C, sysCar.D);
tfCar = tf(num,den);

s = tf('s');
tfCar = 1 / (s+1);

DesiredLoop = 10/s;

K = DesiredLoop / tfCar;

tfCar = 1/(s+.5);

sysLoop = series(K,sysCar); %Serial connection of two models.
sysCL = feedback(sysLoop,1,-1);

step(sysCL);
hold on
step(sysCar)