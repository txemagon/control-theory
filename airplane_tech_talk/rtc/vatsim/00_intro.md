# Communications

## Request IFR clearance


### Standard

- ✈: Cologne Delivery, BER4UX, information alfa, request clearence to munich.
- ⚵: BER4UX, Cologne Delivery, clear to Munich via KUMIK1C departure, flight planned route, climb via SID 5000ft, squawk 1234.
- ✈: BER4UX, clear to Munich via KUMIK1C departure, flight planned route, climb via SID, altitude 5000ft, squawk 1234.
- ⚵: BER4UX, readback correct

### UK

- ✈: Cologne Delivery, BER4UX, information alfa, request clearence to munich.
- ⚵: BER4UX, clear to Munich, KUMIK1C departure, squawk 1234.


## Pushback and Taxi

[DEL]
- ✈: BER4UX, request push back and start up.
- ⚵: BER4UX, start up approved, for pushback contact ground 121.725.

[GND]
- ✈: BER4UX, request push back and start up.
- ⚵: BER4UX, pushback and start up approved facing North.
- ✈: BER4UX, pushback and start up approved facing North.

[Actions]

1.  Turn on the transponder.
1. Use the pushback tool.

[GND]
- ✈: BER4UX, request taxi.
- ⚵:


- ✈:
- ⚵:

