# Aircraft Magnetism

1. Hard Iron: Permanent
1. Soft Iron: Temporary Magnetism

## Deviation

- Angle between compass North and magnetic North.
- Compass swing.

| COMPASS <br> HEADING | MAGNETIC <br> DATUM | OBSERVED <br> DEVIATION |
|------|------|------|
| 000  | 000  |  0   |
| 090  | 096  | +6   |
| 180  | 184  | +4   |
| 270  | 268  | -2   |

1. Longitudinal component. ${ DEV_{long} = Coef_B  \sin (heading) }$
1. Lateral component. ${ DEV_{lat} = Coef_C  \cos (heading) }$
1. Coefficint A represents the missalignment of the compass to the longitudinal axis of the plane.

```math
Deviation(\psi) = Coeff_A + Coeff_B \sin(\psi) + Coeff_C \cos(\psi)
```

The compass swing must be performed:

1. The compass are installed or replaced.
1. The accuracy of the compass is in doubt.
1. After maintenance inspection if required.
1. After significcant modification of the aircraft.
1. Carrying magnetic payloads.
1. When the compass has been subjected to significant shock.
1. When the aircraft operates at new latitudes.
1. When long term storage in a single heading.
1. Lightning strike.
1. After radio or electrical modifications.


