# Grid Navigation Convergence

- Standard Grid. When Greenwich is chosen as datum meridian.
- USA & Canada often choose 60W.
- Grid convergence is teh angular difference between True North and Grid North at any particular point on a grid chart.

- Mnemonic Convergency: EWWE

> Convergence east true least.
> Convergence west true best.

| Grid    | Convergence | True    | Variation | Magnetic | Deviation | Compass |
|---------|-------------|---------|-----------|----------|-----------|---------|
| 090º(G) | 10ºW        | 100º(T) | 8ºW       | 108º(M)  | 2ºE       | 106º(C) |

1. Grivation: Grid Convergence + Variation.
1. Isogrivs: Isogonal + Grid convergence.

1. Residual Transport Wander. Difference between Earth Convergence and Chart Convergence.

## Examples

Over North Pole Stereographic Chart, we see in video 87.

![Polar Plane Grid](./images/polar_plane_grid.png)

If it were the Northern Hemisphere:

| Aircraft | Longitude | Convergence | Grid Hdg | True Hdg |
|----------|-----------|-------------|----------|----------|
| 1        | 090W      | 090E        | 360      | 270      |
| 2        | 045W      | 045E        | 225      | 180      |
| 3        | 045E      | 045W        | 315      | 360      |
| 4        | 090E      | 090W        | 360      | 090      |
| 5        | 135E      | 135W        | 090      | 225      |


If it were the Southern Hemisphere:

| Aircraft | Longitude | Convergence | Grid Hdg | True Hdg |
|----------|-----------|-------------|----------|----------|
| 1        | 090W      | 090W        | 360      | 090      |
| 2        | 045W      | 045W        | 225      | 360      |
| 3        | 045E      | 045E        | 315      | 180      |
| 4        | 090E      | 090E        | 360      | 270      |
| 5        | 135E      | 135E        | 090      | 045      |


## Grid Problems

Related data:

1. The Datum longitude
1. The aircraft longitude
1. Grid Track
1. True Track

### Problem 1

In a Polar Stereographic Chart:

1. Aircraft Position: 45N 110W
1. Grid Track: 132º(G)
1. Datum: 060ºW

What is the True Track?

- Ans: 082º(T)

### Problem 2

In a Polar Sterographic Chart

1. Aircraft Position: 28S 118E
1. True Track: 042º(T)
1. Grid Track = 133º(G)

What is the Datum?

- Ans: 027E

### Problem 3

In a Lambert's chart:

1. Aircraft Position: 50S 178E
1. Parallel of Origin = 56S (n=.82)
1. Grid Track: 192º(G)
1. True Track: 211º(T)

What is the Datum Meridian?

- Ans: 159W

### Problem 4

There are another problems to be solved by plotting.

Assume a Nort Polar Stereographic Chart whose grid is aligned
with the Greenwich meridian. An aircraft then flies from the
north geographic pole for a distance of 480 nm along the
110ºE meridian, then follows a grid track of 154º(G) for
a distance of 300 nm. What is the aircraft's final position?

a. 7845N 087E
a. 8000N 080E <-
a. 7915N 074E
a. 7015N 080E



