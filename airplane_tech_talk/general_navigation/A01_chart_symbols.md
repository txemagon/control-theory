# Chart Symbols

## AERODROMES

| No. | Description                                                 | Symbol                           | 
|-----|-------------------------------------------------------------|----------------------------------| 
| 1   | Civil(land)                                                 | <img alt="civil land" src="./images/chart_symbols/set2/civil_land.png" width="136" >|
| 2   | Military(land)                                              | <img alt="military land" src="./images/chart_symbols/set2/military_land.png" width="136" > |
| 3   | Emergency aerodrome or aerodrome with no facilites          | <img alt="emergency land" src="./images/chart_symbols/set2/emergency_ad.png" width="136" > |
| 4   | Sheltered anchorage                                         | <img alt="sheltered anchorage" src="./images/chart_symbols/set2/sheltered_anchorage.png" width="136" > |
| 5   | Heliport                                                    | <img alt="heliport" src="./images/chart_symbols/set2/heliport.png" width="136" > |

## RADIO NAVIGATION AIDS

| No. | Description                                                 | Symbol                            | 
|-----|-------------------------------------------------------------|-----------------------------------| 
| 1   | Basic radio navigation aid                                  | <img alt="basic aid" src="./images/chart_symbols/set2/basic_radio_navigation_aid.png" width="136" > |
| 2   | Distance Measuring Equipment                                | <img alt="dme" src="./images/chart_symbols/set2/distance_measuring_equipment.png" width="136" > |
| 3   | Collocated VOR and DME radio navigation aids                | <img alt="vordme" src="./images/chart_symbols/set2/collocated_VOR_and_DME.png" width="136" > |
| 4   | VOR and TACAN combination  - VORTAC                         | <img alt="vortac" src="./images/chart_symbols/set2/VORTAC.png" width="136" > |

## AIRSPACE

| No. | Description                                                 | Symbol                            | 
|-----|-------------------------------------------------------------|-----------------------------------| 
| 1   | Compulsory reporting point                                  | <img alt="compulsory reporting" src="./images/chart_symbols/set2/compulsory_rep.png" width="136" > |
| 2   | On request reporting point                                  | <img alt="request reporting" src="./images/chart_symbols/set2/on_request_rep.png" width="136" > |
| 3   | Final approach fix                                          | <img alt="final approach fix" src="./images/chart_symbols/set2/final_approach_fix.png" width="136" > |
| 4   | Compulsory Fly-by                                           | <img alt="compulsory fly-by" src="./images/chart_symbols/set2/compulsory_fly-by.png" width="136" > |
| 5   | Fly-by point                                                | <img alt="fly-by" src="./images/chart_symbols/set2/fly-by.png" width="136" > |
| 6   | Fly-over point                                              | <img alt="fly-over" src="./images/chart_symbols/set2/fly-over.png" width="136" > |
| 7   | Restricted airspace (prohibited, restricted or danger area) | <img alt="restricted" src="./images/chart_symbols/set2/restricted_airspace.png" width="136" > |
| 8   | Common boundary of two areas                                | <img alt="common boundary" src="./images/chart_symbols/set2/common_boundary.png" width="136" > |


## AERODROME CHARTS

| No. | Description                                                 | Symbol                            | 
|-----|-------------------------------------------------------------|-----------------------------------| 
| 1   | Hard surface runway                                         | <img alt="hard runway" src="./images/chart_symbols/set2/hard_surface_RWY.png" width="136" > |
| 2   | Unpaved runway                                              | <img alt="unpaved runway" src="./images/chart_symbols/set2/unpaved_RWY.png" width="136" > |
| 3   | Stopway                                                     | <img alt="stopway" src="./images/chart_symbols/set2/stopway.png" width="136" > |
| 4   | Aerodrome reference point (ARP)                             | <img alt="aerodrome reference point" src="./images/chart_symbols/set2/aerodrome_reference_point.png" width="136" > |
| 5   | Taxiways and parking aereas                                 | <img alt="taxiways and parking" src="./images/chart_symbols/set2/taxiways_and_parking_areas.png" width="136" > |
| 6   | Wind direction indicator (lighted)                          | <img alt="lighted wind indicator" src="./images/chart_symbols/set2/wind_direction_indicator-lighted.png" width="136" > |
| 7   | Wind direction indicator (unlighted)                        | <img alt="unlighted wind indicator" src="./images/chart_symbols/set2/wind_direction_indicator-unlighted.png" width="136" > |
| 8   | Runway-holding position (Pattern A)                         | <img alt="runway holding position pattern A" src="./images/chart_symbols/set2/RWY_holding_PSN-Pattern_A.png" width="136" > |
| 9   | Runway-holding position (Pattern B)                         | <img alt="runway holding position pattern B" src="./images/chart_symbols/set2/RWY_holding_PSN-Pattern_B.png" width="136" > |
| 10  | Runway visual range - RVR                                   | <img alt="runway visual range" src="./images/chart_symbols/set2/RVR.png" width="136" > |
| 11  | Stop bar                                                    | <img alt="stop bar" src="./images/chart_symbols/set2/stop_bar.png" width="136" > |
| 12  | Landing direction indicator (lighted)                       | <img alt="landing direction indicator lighted" src="./images/chart_symbols/set2/LDI-lighted.png" width="136" > |
| 13  | Landing direction indicator (unlighted)                     | <img alt="landing direction indicator unlighted" src="./images/chart_symbols/set2/LDI-unlighted.png" width="136" > |


## OBSTACLES

| No. | Description                                                 | Symbol                            | 
|-----|-------------------------------------------------------------|-----------------------------------| 
| 1   | Obstacle                                                    | <img alt="obstacle" src="./images/chart_symbols/set2/obstacle.png" width="136" > |
| 2   | Lighted Obstacle                                            | <img alt="lighted obstacle" src="./images/chart_symbols/set2/lighted_obstacle.png" width="136" > |
| 3   | Group Obstacles                                             | <img alt="group obstacles" src="./images/chart_symbols/set2/group_obstacles.png" width="136" > |
| 4   | Lighted Group Obstacles                                     | <img alt="lighted group obstacles" src="./images/chart_symbols/set2/lighted_group_obstacles.png" width="136" > |
| 5   | Exceptionally high obstacle - lighted                       | <img alt="excepionally high obstacle" src="./images/chart_symbols/set2/exceptionally_high_obstacle.png" width="136" > |
| 6   | Elevation of top obstacle above sea level (italics) <br width="136" > Height above specified datum (upright type in parenthesis)         | <img alt="top obstacle elevation" src="./images/chart_symbols/set2/ELEV_of_top_of_OBST.png" width="136" > |


## MISCELLANEOUS

| No. | Description                                                 | Symbol                            | 
|-----|-------------------------------------------------------------|-----------------------------------| 
| 1   | International boundary                                      | <img alt="international boundary" src="./images/chart_symbols/set2/international_boundary.png" width="136" > |
| 2   | Transmission line or overhead cable                         | <img alt="cable" src="./images/chart_symbols/set2/transmission_line.png" width="136" > |
| 3   | Isogonal                                                    | <img alt="isogonal" src="./images/chart_symbols/set2/isogonal.png" width="136" > |


# SUMMARY

![Summary](./images/chart_symbols/summary.png)
