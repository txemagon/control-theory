# Navigation Plotting


Some considerations:

| Option A | Option B |
|----------|----------|
| Relative | Absolute (with respecto to north) |
| True | Magnetic |
| Measured at aircraft | Measured at Ground Station |


- QDM: Magnetic Heading TO VDF
- QDR: Magnetic Heading FROM to VDF
- QTE: True Bearing. VDF response to a pilot transmission.

RMI: Radial Magnetic Indicator. VOR and NDB

![Radial Magnetic Indicator](./images/radial_magnetic_indicator.png)

RBI: Relative Bearing Indicator. NDB

![Radial Magnetic Indicator](./images/relative_bearing_indicator.png)

QDR is needed for plotting
