# Time

## Units

1. Standard Time, not local time.

Kepler's 1st Law:

> The orbit of each planet is an ellipse with the sun at one of the foci.

1. Avg distance to Sun: 93E6 sm / 150E6 km
1. Perihelion - Closest (4th January). Dist: 91.4E6 sm.
1. Aphelion - Furthest (3rd July). Dist: 94.6E6 sm.

Kepler 2nd Law:

> The line joining the planet to the sun, the radius vector, sweeps out equal areas in equal time.

This has implications on day length.

Apparent Solar Day:

1. Sidereal Day.
1. Apparent Solar Day -- apparent solar transit over a meridian --. Greater than Sidereal Day.
1. Apparent Solar Day ranges ${ 23\frac{3}{4} }$ and ${ 24\frac{1}{4} }$

Average Day:

```math
AvgDay = \frac{year \, length}{365 \frac{1}{4}}
```

This is Greenage Mean Time and gives us the Civil Day.

Based on Cesium whe have Co-ordinated Universal Time (UTC), pretty much the same as GMT.
The differ in 1 second per year.

The Equation of Time shows UTC vs Meridian Transit

The year:

- Sidereal: 365 days and 6 hours.
- Tropical year: 365 days 5 hours 48 minutes 45 seconds
- Every four years is leap, unless is a not fourth century.

## Seasons

- Tilt 23.5º
- Ecliptic. Earth - Sun plane
- Equinoctial. Perpendicular to the spin axis.
- Oblicuity of the Eclipltic = angle between Ecliptic and Equinoctial = 23.5º
- The angle between the spin axis and the ecliptic is 66.5º.
- Depending of the season the line of the sun is:
    - Winter Solstice (Dec 21). Sun over 23.5S
    - Spring (Vernal) Equinox (March 21). Sun over Equator.
    - SummerSolstice (June 21). Sun over 23.5N
    - Autumn Equinox (Sept 21). Sun over Equator.
    
 
 > Declination is the angle measured northwards or suthwards from the equinoctial to
 > a line joining the centre of the earth to a body in space.

Intertropical Zone. Shadow North and South. 
 - Tropic of Cancer: 23.5ºN.
 - Tropic of Capricorn: 23.5ºS.

Midnight Noon:
 - Artic Circle: 66.5ºN.
 - Antartic Circle: 66.5ºS.
 
## Local Mean Time

Civil (apparent Solar) Day:

- 24h       = 360º rotation.
- 1 hour    = 15º rotation.
- 4 minutes = 1º rotation
- 1 minute  = 15' of arc.

When crossing the antimeridian from West to East -> +1 day.

Example:

| Longitude | Hours | Minutes | Seconds |
|-----------|-------|---------|---------|
| 137º      | 9     |  8      | 0       |
|  36'      | 0     |  2      | 24      |
|  137º 36' | 9     | 10      | 24      |


> Longitude west, UTC best, Longitude east, UTC least.

Or

| New York | London | India |
|----------|--------|-------|
| Breakfast <br> (8 am) | Lunch <br> (1 pm) | Tea <br> (6.30 pm) |

### Problems

#### Problem 1

What is the LMT at 103º 55'E if the UTC is 0800h?

- Ans: 14h55m40s

#### Problem 2

The LMT at A, longitude 137º 50'W is 1812 on the 18th August.
What is the LMT at B, longitude 121º 12'E?

- Ans:  11h28m, 19th August


## Standard Time

UK:

1. UTC in Winter.
1. UTC+1 in Summer.

1. World Standard Times not Time Zones.
1. Some isles don't want to change the day, so there are 13h time differences and day change line differs from the antimeridian.
1. Some countries have several standard time.
1. An asterisk in the time almanac indicates the country operates daylight saving time. UTC +1 between april and october.


## Sunrise and Sunset

The position of the sun for calculating sunrise and sunset:

1. The center of the sun is not the datum for sunrise and sunset,
but the top tip of it. The sun measures 32' of arc. The semidiameter: 16' of arc.
1. There is atmospheric refraction of 24' of arc at sunset and sunrise. The straight line is the Sensible Horizon and differs from the Visual Horizon.
1. There is 50' of arc from the Visual Horizon to the Sensible Horizon. So there is always more than 3 minutes in time between them.

Finally, time
1. from Sunrise to Sunset at equinox = 12h 06m 
1. below horizon at equinox = 11h 54m

Twilight occurs in 3 phases:

1. During Civil Twilight tasks, such as takeoff and landing can be undertaken.
    + Evening civil twilight: The center of the sun is in the margin of 6º below the sensible horizon.
    + Morning civil twilight: The center of the sun is 6º below Sensible Horizon.
1. Nautical Twilight: Between 6º and 12º. The Horizon is still visible.
1. Astronomical Twilight: 12º - 18º. Is darkness.

Aviators only operates in the period of civil twilights.

At an Equinox, the sunset occurs 50'arc below horizon. To the 6º below, 5º10' remain. At 15º/h, minimum civil twilight time is 21 minutes.

The duration of civil twilight is gathered in the time air almanac.

