# Plotting

## Point of Equal Time

### Names

1. Point of Equal Time (PET)
1. Equal Time Point (ETP)
1. Critical Point (CP)

PET: Time on = Time home.

### Examples

#### Example 1

+ A -> B: 400nm
+ TAS: 200 kt
+ Still Air

-Ans: PET = 200 nm

#### Example 2

All data remains equal, but:

+ there is 50kt Head Wind

So, 
1. G/S home = 250 kt
1. G/S onwards = 150 kt

- Ans: PET = 250 nm from departure.

#### Example 3

+ Tail Wind: 30 kt

So,

1. G/S onwards: 230 kt
1. G/S home: 170 kt.

- Ans: PET = 170 nm.

### Formula

Let,  
 D = dist(A,B)  
 X = PET  
 H = Homewards GroundSpeed  
 O = Onwards GroundSpeed  
 
 ```math
 \frac{X}{H} = \frac{D-X}{O}
 ```
 
 ```math
 X = D \frac{H}{O+H}
 ```
 
#### Problems

##### Problem 1

+ D = 500 nm
+ TAS = 300 kt
+ Still air.

-Ans: PET = 250 nm

##### Problem 2

+ 60 kt headwind
H = 360 kt kt
O = 240 kt

-Ans: PET = 300 nm


##### Problem 3

+ 60 kt tailwind

- Ans: PET = 200 nm

### Non Constant Problems

Non Constant Wind

+ D = 2050 nm
+ TAS = 475 kt

From the PET, 10kt tailwind.
+ O = 465 kt
+ H = 430 kt

- Ans: PET = 985nm 

Non constant TAS

One engine out CP

### Conclusions

1. The CP / PNR always moves toward the wind is blowing.


## Point of No Return

Or Point of Safe Return

Fuel Requirements:

1. 30m Reserve
1. Contingency Fuel

Before the Point of Safe Return,

1. Check weather conditions.
1. Ensure Runway is available.


With 10h fuel, the PSR is 5h in still air. At 300kt -> 1500nm.

### Formula

+ E:Safe Endurance. Fuel used to flight wihout reserves.
+ TE: Total Endurance. Fuel to dry tanks.
+ O: Outbound Groundspeed.
+ H: Homebound Groundspeed.
+ T: Time to turn.
+ Distance out = Distance Home
+ O x T = H x (E - T)

```math
T = E \frac{H}{O + H}
```

+ PSR = OT

#### Examples

| Data | Value  |
|------|--------|
| TE   | 11 h   |
| Alt  | 1/2 h  |
| FRes | 1/2 h  |
| Cont | 0 h    |
| E    | 10 h   |
| TAS  | 300 kt |

##### Still Air

1. T = 5h
1. PSR = 1500 nm

##### 50 kt Headwind Outbound

- O = 250 kt
- H = 350 kt

1. T = 5,83
1. PSR = 1458 nm

##### 50 kt Tailwind Outbound

- O = 350 kt
- H = 250 kt

1. T = 4.16h
1. PSR = 1458nm

### Conclusions

1. Head / Tail wind always reduce the distance to PSR


### Calculations

#### Problem 1

Given:

+ TE: 7h40m
+ Required Reserves: 1h40m
+ TAS: 200 kt
+ D: 1200nm
+ Outbound W/C = + 30 kt constant

Time and distance to PSR:

a. 2h33m 587nm <-
a. 3h15m 750nm
a. 2h27m 794nm
a. 2h33m 434nm

### Alternate Formula

+ d: distance to PSR
+ F: Safe Fuel (quantity)
+ CO: Fuel Consumption Outbound (kg/nm)
+ CH: Fuel Consumption Homebound (kg/nm)

```math
d = \frac{F}{CO + CH}
```

+ F = 39500kg
+ TAS = 310 kt
+ W/C outbound = + 30 kt
+ W/C homebound = -30kt
+ FFout = 6250 kg/h
+ FFhome = 5300 kg/h

Calculations:

+ O = 340 kt G/S
+ H = 270 kt G/S
+ CO = FFout / O = 18.382 kg /ngm
+ CH = FFhome / H = 18.929 kg/ngm
+ CO + CH = 37.31 ngm

Finally

1. d = 1058.7 nm
1. T = 1058.7 / 340 = 3h07m



