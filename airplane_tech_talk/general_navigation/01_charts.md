# General Navigation

Get tested at [brainscape](https://www.brainscape.com/packs/general-navigation-10964028)


# Introductory Concepts

## Great Circle Vertices

They are antipodal: 63N 10E -> 63S 170W  

Properties:

1. GC crosses the equator at 90º from vertices: 100E, 80W
1. GC crosses the equator at an angle equal to the latitude of the vertices. 63º
1. The track on vertex is 090º or 270º.




## Measure & Distances:

1. 10,000 km = distance from pole to the equator. => equator = 40,000 km
1. 1 minute of arc = 1 nm. 9000N E/W00000 <- 5128N E/W00000 = 38x60 + 32 = 2312 nm


## Magnetism

1. Rhumb lines: Constant Track.
1. Magnetic North 22 miles/year.
1. 2005: MNorth 83ºN 114W, 420nm distance to the True North
1. Isogonals: Lines of equal magnetic **variation**.
1. Agonic line: 0º isogonal.
1. Angle of dip = inclination. DIP=0º is Magnetic equator.
1. Aclinic line = 0º Inclination
1. Directive Force = Horizontal component of magnetic force
1. Magnetic Deviation: Errors due to secondary magnetic fields.

To compensate magnetic deviation:

Pending:




## Factors

| angle  | 0º  | 15º   | 30º   | 45º    | 60º   | 90º |
|--------|-----|-------|-------|--------|-------|-----|
| sine   | 0.0 | 0.259 | 0.5   | 0.7071 | 0.866 | 1.0 |
| cosine | 1.0 | 0.966 | 0.866 | 0.7071 | 0.5   | 0.0 |

## Convergency / Earth Convergence

1. Def 1. Angle between to given meridians at a given latitude, ϕ.
1. Def 2. Change in _Great Circle_ direction between two points.
1. At the pole convergy between to meridians equals to the difference in longitude.

```math
convergency = \Delta \lambda \cdot \sin \bar{\phi}
```

![Convergency](./images/convergency.svg)


### Problems

#### Problem 1

The intial Great Circle track from A (40N 002W) to B (50N 010E) is 060ºT.
What is the initial Great Circle track from B to A?

Ans: 248.5

#### Problem 2

The initial Great Circle track from C (3600N 01500E) to D (4200N) is 300ºT and
the final Great Circle track at D is 295ºT.

What is the longitude, λ, of D? - 007E
What is the GC track direction at λ=01100E? 297.5º


#### Problem 3

The intial Great Circle track from H(40S 170W) to G(45S 174E) is 250ºT.

What is the initial great Circle track from G to H? - Ans: 80.º


## Conversion Angle

1. Difference between GC direction and Rhumb line direction joining two points.
1. In the middle GC and Rhumb have the same direction
1. Is half convergency.


### Problems

#### Problem 1

Determine teh value of convergency between J(5812N 00400W) and K (5812N 00600E). Ans: 8.5º
What is the Rhumb line from J to K? Ans: 90ºT
What is the initial Great Circle track from K to J? ANs: 274.25ºT

#### Problem 2

A is at 55N 000E/W and B is at 54N 010E. If the initial true Great Circle track from A to B is 100ºT,
what is the Rhumb Line track at A? - Ans: 104ºT


#### Problem 3

The following waypoints are entered into an inertial navigation system (INS):

1. WPT 1: 53º08'N 030W
1. WPT 2: 53º08'N 020W
1. WPT 3: 53º08'N 010W

The inertial navigation system is connected to the automatic pilot on the route
WPT 1 - WPT 2 - WPT 3. What will be the approximate track change on passing WPT 2?

Ans: 8º

## Departure

Distance along Great Circle points:

```math
GCd = \arccos \left[ \sin \phi_1 \sin \phi_2 + \cos \phi_1 \cos \phi_2 \cos(\lambda_1 - \lambda_2 ) \right ]
```

On the same meridian or on the equator: 

```math
1º = 60nm
```

Over any other parallel (a Small Circle) the distance is called **Departure**:

```math
Dep(nm) = \Delta \lambda(min) \cdot \cos \phi
```

1. Departure is always a Rhumb line.

The following equation is true whenever chLong is kept:

```math
\frac{Dep(\phi_A)}{cos \phi_A} = \frac{Dep(\phi_B)}{cos \phi_B}
```

### Problems

#### Problem 1

An aircraft at position 60N 005 22W flies 165km due East. What is the new position?

- Ans: 60N 002 24W



#### Problem 2

In which latitude is a difference of longitude of 44º11' equivalen to a departure of 2000 nm?

- Ans: 41ºN/S

#### Problem 3

An aircraft leaves position G at latitude 40ºS and flies the following Rhumb line tracks and distances:

1. G to H (180ºT - 240nm)
1. H to J (270ºT - 240nm)
1. J to K (000ºT - 240nm)

What is the Rhumb Line bearing and distance from K to G?

Ans: 270ºT - 255.6nm

#### Problem 4

An aircraft at position 2700N 17000W travels:

1. 3000 km - 180ºT.
1. 3000 km - 090ºT.
1. 3000 km - 000ºT.
1. 3000 km - 270ºT.

What is the final position? 

Ans: - 27º00'N 173º18'W


## Scale

| source | Destination | Destination |
|--------|-------------|-------------|
| 1 nm   | 6080 feet   | 1852m       |
| 1 m    | 3.281 feet  |             |
| 1 feet | 12 inches   |             |
| 1 inch | 2.54 cm     |             |

- 10 km = 5.4 nm. (10,000 km = 90º x 60 nm/º) = 5400nm

Examples:

1. 1 inch = 20 nautical miles
1. Graduated scale lines. If not present, it can be measured against minutes of change of latitude.
1. A fraction: chart length / Earth distance. 1: 500,000. Same units --Only works for metric system, 1:729600 means 1 inch = 10nm --.


### Problems

#### Problem 1

On a particular chart, 5 cm represents 7 nm. What is the scale?

- Ans: 1: 259280

#### Problem 2

On a chart of scale 1:5,000,000
how many nautical miles on the Earth are represented by 1.7cm?

- Ans: 45.89nm

#### Problem 3

A chart has a Representative Fraction of 1:2,000,000. What chart
length in inches represents an Earth Distance of 250 km?

- Ans: 12cm = 4.92 inches


- Large Scale Maps: Highly detailed.


# Charts


## Types of Projection Surfaces:

1. Cylindrical -> Mercator
1. Conical -> Lambert
1. Azimuthal / Plane -> Polar stereographic


## Properties

Desirable properties:

1. Scale should be constant. Very desirable.
1. Conformality: Angles are preserved. aka Orthmorphism. That is the one that matters.
1. Areas remain equal.
1. Shapes are equal.

We'd also like that:

1. Rhumb lines were straight lines.
1. Great Circles were straight lines.
1. Latitude and longitude were easy to plot.
1. Adjacent sheets fit correctly.
1. Worldwide coverage.

For conformality it's needed:

1. Parallels and meridiations cut at 90º.
1. Scale must change at an equal rate in all directions.



## Map types

1. Mercator
1. Lambert's conformal
1. Polar Stereographic
1. Transverse and Oblique Mercators

### Mercator

- Rhumb Lines are straight lines
- Orthomorphic

Cylindrical Projection:
- E/W Scale: Proportional to sec lat
- N/S Scale: Proportional to tg lat

Mercator adjusted the N/S scale to make it proportional to the sec lat.

```math
S(\phi) = \frac{S(Eq)}{\cos \phi} = S(Eq) \sec \phi
```

- Orthomorphic: Due vertical adjustment IS orthomorphic.
- Graticule: 8ºN/S -> 1% of scale change, taken as constant scale. 6ºN/S -> 0.5%. Use local latitude scale.
- Shapes: Too wide in the higher latitudes.
- Chart Convergence: 0º. Correct at the equator only.
- Rhumb Lines: Straight.
- Great Circles: Concave to the equator. Crossing the equator S shape to maintain concavity.


| Property | Features |
|----------|----------|
| Scale | Correct at the equator. <br> Elsewhere it expands as the secant of the latitude. <br> Whitin 1% up to 8º from the equator. <br> Within 1/2% up to 6º from the Equator |
| Orthomorphism | Yes. All charts used for navigation must be. |
| Graticule | Meridians are straight parallel lines, evenly spaced. <br> Parallels are straight parallel lines with the space between them increasing as teh secant of the latitude. |
| Shapes | Correct over small areas. <br> Big distortion at high latitudes. |
| Chart Convergence | Zero everywhere. <br> Correct at the Equator. <br> Constant across the chart. |
| Rhumb Lines | Straight lines. Always. Everywhere. |
| Great Circles | The equator and meridian are straight lines. <br> Concave to the equator. |


#### Required Skills

1. Given scale at the Equator, calculate Mercator Scale at some other latitude.
1. Given Mercator Scale at some latitude, calculate the Scale at the Equator.
1. Given Mercator Scale at some latitude, calculate the Scale at some other latitude.
1. Given a fixed chart length between meridians, calculate Mercator Scale at a specific latitude.


#### Normal Mercator Plotting

Some radio aids:

1. VOR. Very High Frequency Omni-Directional Range.
1. VDF. Very High Frequency Direction Finding.
1. NDB/ADF. Non Directional Beacons / Automatic Direction Finding.

Types of waves,

in vertical direction:

1. Space Wave
1. Sky Wave
1. Surface Wave.

in horizontal direction, the always follow a Great Circle. So, for instance,
if the convergency between a transmitter in Oxford and a receiver is 4º, a 
radio beam departing from Oxford at 240ºT QTE and arriving at the aircraft at 236ºT
needs some conversion before plotting.
To find Oxford on a Mercator chart a Rhumb Line must be plotted. Using the
conversion angle, it has to be drawn 238ºT.

The conversion angle must be applied where the angle is measured.


### Lambert's Conical Conformal

Mercator,
1. doesn't show GC as lines.
1. Scale varies.

#### Conical projection

1. is not Orthomorphic.
1. has a parallel of tangency/origin.
1. The angle at the apex of the cone is always twice the lattitude at the parallel of tangency.
1. Unfolded the cone a closed RL in the earth doesn't show up closed in the map, ${ map_arc = \Delta \lambda \cdot \sin ( \phi_{po} ) }$, being po the parallel of origin.
1. If PO = 45º, chLon = 100º in the chart would be 100 sin(45) = 70.71º and is called chart convergence.
1. sin (PO) is called constant of the cone.


#### Lambert's Changes

1. Intersection of the cone at 2 standard parallels.
1. The constant of the cone does not change.
1. The scale expansion in the first and last sixth of the map equals the scale contraction of the middle sixth. 0.1% in many occasions.

#### Properties

| Property      | Features |
|---------------|----------|
| Scale         | Correct on the standard parallels. Contracted within. |
| Orthomorphism | Yes. |
| Graticule     | Meridians straight. Parallels circles centered at the pole. |
| Shapes        |  |
| Chart Convergence | Constant. <br> Determined by the parallel of origin, mean of the standards. <br> ${ cc = \Delta \lambda \cdot \sin ( \phi_{po} ) }$ |
| Rhumb Lines   | Meridians - straight lines. The rest, concave to the pole. |
| Great Circles | Meridians - straight lines. The rest, a curve concave to the parallel of origin. |

#### Problems

##### Problem 1

A straight line on a Lambert's chart crosses the twelve degrees East meridian at A, which is 40ºN, on
a track angle of 292ºT. It crosses another meridian at B, further west on a track angle 283ºT. The sine
of the parallel of origin is 0.60. What is the longitude of B?

- Ans: -15º / 3W

##### Problem 2

Half chart convergence:

```math
hcc = \frac{1}{2} \Delta \lambda \cdot \sin ( \phi_{po} )
```

On a Lambert's chart, the constant of the cone is 0.80. A is at 53N 004W. You plan to fly to B. The initial Lambert's straight line track is 070ºT and the Rhumb line track from A to B is 082ºT. 
What is the longitude of B?

- Ans: 026ºE

#### Lambert's Conformal Plotting

- Great Circles and Radio aids can be plotted without conversion angles.
- When direction is measured on ground (VOR - VDF) there is no need for correction.
- When measured on aircraft (NDB( ADF - AWR). Plotting from the station shall take chart convergence into account, or use a false meridian.

### Polar stereographic

Scale expansion:

```math
SE = \sec^2(\frac{1}{2} colat)
```

- 78ºN => 1% scale error. 1440nm. (3h at 450 kt)
- 70ºN => 3%. 2400nm (5h at 450 kt)


![Polar Stereographic North Pole](./images/polar_stereographic_N.svg)

Scale:

```math
S(\phi) = Scale \cdot \sec^2(\frac{colat}{2})
S(\phi) = Scale \cdot \sec^2(\frac{\pi/2 - \phi}{2})
```

| Property      | Features |
|---------------|----------|
| Scale         | Correct at Pole. <br> Elsewhere expands as ${ \sec^2 (\frac{1}{2} colat) }$. <br> Within 1% error till 78º. <br> Within 3% from 78ºN to 70ºN. |
| Orthomorphism | Yes. And perspective. |
| Graticule     | Meridians are straight lines. <br> Parallels are concentric circles centred on the Pole. |
| Shapes        | More distorted as the distance increases from the Pole. |
| Chart Convergence | Correct at the Pole. <br> Constant across the chart. <br> cc = change of longitude. <br> ${ n=1 }$ |
| Rhumb Lines   | Curves concave to the nearer Pole. |
| Great Circles | Curves concave to the nearer Pole. <br> With less curvature than Rhumb Lines. <br> Become more straight closer to the Pole. <br> Can be taken as straight lines at latitudes greater than 70º. |

- Track change is equivalent to the change of longitude

#### Problems

##### Problem 1

A flight from Miami 083ºW to the Southern tip of India 085ºE. chLong=168º Eastwards.
The initial track is 004ºT. What is the final track direction?

- Ans: 172ºT


##### Problem 2

We shall go from Cape Town 022ºE to New Zealand 175ºE. chLong=153º Eastwards. The initial track is 169ºT.
What is the final track direction?

- Ans: 016ºT

##### Problem 3

What is the initial straight-line track from A(75N 060W) to B at (75N 060E)?

a) 090ºT
b) 030ºT <-
c) 120ºT
d) 330ºT

From B to A would have been 330ºT. And the arrival track direction would be the opposite of
330ºT or the initial plus the chLong = 150ºT.

Also it can be solved in a cumbersome manner. The RL would have been 90ºT. The convergency angle is 60º. So if
the backwards RL is 270º and the convergence angle is 60ºT, then backtrack must be 330ºT.

##### Problem 4

At what longitude does the straight-line track from A (70N 40W) to  B (70N 80E) on a Polar Stereographic
chart reach its highest latitude?

a) 040ºW
b) 030ºW
c) 020ºE <-
d) 040ºE

##### Problem 5

Using GC Vertices...

On a Polar Stereographic chart, a line is drawn from position A(70N 102W) to B (80N 006E). The point of 
highest latitude along this line occurs at 035ºW. What is the initial straight-line track angle from 
A to B, measured at A?

a) 049ºT
b) 077ºT
c) 229ºT
d) 023ºT <-


### Transverse Mercator


| Property      | Features |
|---------------|----------|
| Scale         | Correct along the Datum Meridian. <br> Elsewhere expands as secant GC E/W arc distance. |
| Orthomorphism | Yes. |
| Graticule     | Complex. <br> Datum Meridian, meridian perpendicular to it and the Equator are straight lines. <b> Other meridians are complex curves. <br> Parallels are ellipses (Eq straight) |
| Shapes        | Distortion increases with the East/West distance from the Datum Meridian. |
| Chart Convergence | Complex. <br> Correct at the Pole and the Equator. |
| Rhumb Lines   | Complex curves.<br> Except the Datum Meridian, the meridian perpendicular to it and the Equator. |
| Great Circles | Complex curves, except for: <br> Datum Meridian. <br> Meridian perpendicular to it. <br> The Equator. <br> Any Great Circle perpendicular to the Datum Meridian. |

### Oblique Mercator

| Property      | Features |
|---------------|----------|
| Scale         | Expand as the secant. Almost constant near the False Equator. |
| Orthomorphism | Yes. |
| Graticule     | Complx, but Orthomorphic. |
| Shapes        | As Mercator. |
| Chart Convergence | Correct along Great Circl of Tangency, Poles and Equator |
| Rhumb Lines   | Complex Curves. |
| Great Circles | Complex. <br> Straight lines close to the Great Circle of Tangency. |
