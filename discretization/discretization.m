%% From Discrete control #2 Discretize! Going from continuous to discrete domain

s = tf ('s');
G = 1/(s+1);
Ts = 0.1;

% Discretization with c2d
Gz = c2d(G,Ts, 'zoh');
Gf = c2d(G,Ts, 'foh');
Gi = c2d(G,Ts, 'impulse');
Gt = c2d(G,Ts, 'tustin');
Gm = c2d(G,Ts, 'matched');

% bode (G, Gz, Gf, Gi, Gt, Gm)
% legend ('Continous', 'ZOH', 'FOH', 'Impulse', 'Tustin', 'Matched')
% figure; step(G, Gz, Gf, Gi, Gt, Gm)
% legend ('Continous', 'ZOH', 'FOH', 'Impulse', 'Tustin', 'Matched')

% figure; impulse (G, Gi, Gz)
% legend ('Continous', 'Impulse', 'ZOH')

% step (G, Gi)  % Doesn't match
% legend ('Continous', 'Impulse')
% step (Gm, Gz) % They do match
% legend ('Continous', 'ZOH')

% Y(z) / V(z) = (z-1) / z * Z{ G(s) / s }t= KT
%% From Discrete control #4: Discretize with the matched method

s = tf ('s');
G = zpk ([], [-1, -2], 3);
dcgain (G)

z = tf('z', 1); % z-domain, 1 second
Gz = 1 / ((z-0.3679) * (z-0.1353) );

% bode(G, Gz)
% figure; step(G, Gz)

Gz_proper = (z+1) / ( (z-0.3679) * (z-0.1353) )
%bode (G, Gz, Gz_proper)
%figure; step (G, Gz, Gz_proper)

Gz_matched = c2d(G, 1, 'matched');
% bode (G, Gz_matched)
% figure; step (G, Gz_matched)


%% From Discrete control #5: The bilinear transform

s  = tf ('s');
G  = 2 / (s + 1);
Gz = c2d (G, 0.1, 'tustin');

% bode (G, Gz)
% figure; step (G, Gz)

%% From Discrete control #6: z-plane warping and the bilinear transform

% Notch discrete filter
s  = tf('s');
T  = 2;        % Sample time
W0 = 0.7;      % Critical frequency
Q  = 1;        % Quality factor

% S-Domain notch filter

G1 = (s^2 + W0^2) / (s^2 + W0/Q*s + W0^2);

% bode (G1)

G1t = c2d (G1, T, 'tustin');
% bode (G1, G1t);


% Discrete frequencies
% z = (1 + sT/2) / (1 - sT/2)
% s = jw
% z = exp(sT)
% 
% exp(jWdT) = ( 1 + jWaT/2) / (1-jWaT/2)

Wa = 2.5;
Wd = 1 / (2*i) * log( (1+Wa*i) / (1-Wa*i) );

% Wa = 2/T tan(Wd T/2)
% Wd = 2/T atan (Wa T/2)

% Frequency prewarped with 
% s -> 2 / T * (z - 1) / (z + 1)
% s -> Wo / tan (W0 T / 2) * (z - 1) / (z + 1)

G1t = c2d(G1, T, ['Method', 'tustin', 'PrewarpFrequency', W0])

bode (G1, G1t)

