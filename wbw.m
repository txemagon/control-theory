         function[] = wbw()
         DR = input('Damping Ratio?    ');
         n = 0;
         n = input('Settling Time(0) or Peak Time(1)?   ');
         if n == 0
         ts = input('Settling Time?   ');
         temp1 = sqrt((4*DR^4) - (4*DR^2) +2);
         temp2 =  1- (2*DR^2);
         temp3 = 4/(ts*DR);
         Wbw = temp3*sqrt(temp1 + temp2)
         end
         if n ==1
         ts = input('Peak Time?   ');
         temp1 = sqrt((4*DR^4) - (4*DR^2) +2);
         temp2 =  1- (2*DR^2);
         temp3 = ts*sqrt( 1- DR^2);
         temp4 = pi/temp3;
         Wbw = temp4*sqrt(temp1 + temp2)
         end
         