# Control Systems in Practice

## Gain Schedule

1. Transient-free Switch: Like fuzzy logic to connect to set of constants
1. Switch all together: Define a curve between states. (systune, controlSystemTuner). Define the constant of a polynomial.

See:
- <https://es.mathworks.com/help/slcontrol/ug/designing-a-family-of-pid-controllers-for-multiple-operating-points.html>

## Feedforward Control
