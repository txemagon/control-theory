# PID Design

## Root Locus


![PID System](./images/PID.png)

## Ziegler-Nichols


1. Start Kp small
1. Increase Kp until neutral stability.
1. Record critical/ultimate gain ku=kp at neutral stability.
1. Record criticla period  of oscillation, Tu (seconds).
1. Loopk up Kp, Ti, Td
1. COmpute Ki and Kd from table or equations

![Neutral Stability](./images/neutral_stability.png)

| type                | Kp     | Ti     | Td     | Ki=Kp/Ti   | Kd=TdKp     |
|---------------------|--------|--------|--------|------------|-------------|
| classic PID         | 0.6Ku  | Tu/2   | Tu/8   | 1.2Ku/Tu   | 0.0075KuTu  |
| P                   | 0.5Ku  | -      | -      | -          | -           |
| PI                  | 0.45Ku | Tu/1.2 | -      | 0.54Ku/Tu  | -           |
| PD                  | 0.8Ku  | -      | Tu/8   | -          | 0.1KuTu     |
| Pessen Inteegration | 0.7Ku  | 2Tu/5  | 3Tu/20 | 1.75Ku/Tu  | 0.105KuTu   |
| Some Overshoot      | Ku/3   | Tu/2   | Tu/3   | (2/3)Ku/Tu | (1/9)Ku/Tu  |
| No Overshoot        | 0.2Ku  | Tu/2   | Tu/3   | (2/5)Ku/Tu | (1/15)Ku/Tu |

Advantages:

- Does not require expert knowledge.
- Does not require model or simulation.
- Good starting point for other techniques.

Disadvantages:

- Not mathematically rigorous.
- Requires a stable system.
- Requires a real system.
- Requires a system that can be driven unstable with increaseing proportional gain.

```matlab
[Kp, Ki, Kd] = ZieglerNichols(Ku, Tu, 'ClassicPID')
```
