# Flight Mechanics 3

## Computing North, East, Down Position

Use DCM to obtain ${ C_{v/b} }$


### Climb Angle

${ \gamma }$ can now be calculated as:

```math
\gamma = \arctan \frac{-V_D}{\sqrt{V_N^2 + V_E^2}}
```

### Course Angle

${ \chi }$ the course angle:

```math
\chi = \frac{\pi}{2} - \arctan \frac{V_N}{V_E}
```
