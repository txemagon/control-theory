\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{ifthen}
\usepackage{pgfplots}
\usepackage{tikz}

%\pgfplotsset{compat=1.8}

%opening
\title{Research Civil Aircraft Model}
\author{Imasen}

\begin{document}

\maketitle

\begin{abstract}
Mathematical model of the a Civil Aircraft based on the work of the emph{Group for Aeronautical Research and Technology in Europe}, \emph{GARTEUR} and professor Christopher Lum. Data provided in th ``Robust Flight Control Design Challenge''
\end{abstract}

\section{Presentation}

\subsection{The model}

\begin{table}[h!t]
\begin{center} 
\begin{tabular}{c|cll}
Property & Value    & Units \\ \hline
$m$     & 120000    & $kg$  &  Mass  \\
$\bar{c}$  & 6.6       & $m$   & Mean aerodynamical chord   \\
$l_t$   & 24.8      & $m$   & Distance to the tail. \\
$S$     & 260.0     & $m^2$ & Wing planform area \\
$S_t$   & 64        & $m^2$ & Wing tail area \\

\end{tabular} 

\caption{Physical properties of the plane.}
\end{center}
\end{table}

The source document describes the plant model, sensors, actuators and control design. Nevertheless we will only cover the plant model.


\subsection{Equations}

Inputs:

$$
\vec{u} = 
\begin{bmatrix}
 \delta_A \\
 \delta_T \\
 \delta_R \\
 \delta_{Th1} \\
 \delta_{Th2} \\
\end{bmatrix}
=
\begin{bmatrix}
 u_1 \\
 u_2 \\
 u_3 \\
 u_4 \\
 u_5 
\end{bmatrix}
$$

State:
$$
\vec{x} = 
\begin{bmatrix}
 u \\ v \\ w \\
 p \\ q \\ r \\
 \phi \\ \theta \\ \psi
\end{bmatrix}
=
\begin{bmatrix}
 x_1 \\ x_2 \\ x_3 \\
 x_4 \\ x_5 \\ x_6 \\
 x_7 \\ x_8 \\ x_9 
\end{bmatrix}
$$

Acceleration, angular moments and Euler kinematical eq:

$$
\dot{\vec{x}} = f(\vec{x}, \vec{u}) = 
\begin{bmatrix}
 \frac{1}{m} \vec{F}^b - \vec{\omega}_{b/e}^b \times \vec{V}^b \\
 I_b^{-1} \left( \vec{M}^b - \vec{\omega}_{b/e}^b \times I_b \vec{\omega}_{b/e}^b \right) \\
 H(\Phi) \vec{\omega}_{b/e}^b
\end{bmatrix}
$$

\subsection{Steps}

\begin{enumerate}
 \item Control Limits / Saturation.
 \item Intermediate Variables.
 \item Nondimensional Force Aerodynamic Coefficients in $F_s$.
 \item Aerodynamic force in $F_b$.
 \item Nondimensionsional Aerodynamic Moment Coefficients about aerodynamic center in $F_b$.
 \item Aerodynamic moment about aerodynamic center in $F_b$.
 \item Aerodynamic Moment about  CoG in $F_b$.
 \item Propulsion effects.
 \item Gravity effects.
 \item Explicit first order form.
 
\end{enumerate}


\section{Control Limits / Saturation}

$$
\vec{u} = 
\begin{bmatrix}
 -25^\circ < \delta_A < 25^\circ \\
 -25^\circ < \delta_T < 10^\circ \\
 -30^\circ < \delta_R < 30^\circ \\
 0.5^\circ < \delta_{Th1} < 10^\circ \\
 0.5^\circ < \delta_{Th2} < 10^\circ \\
\end{bmatrix}
$$


\section{Intermediate Variables}

Airspeed:
$$ V_A = \sqrt{u^2 + v^2 + w^2} $$
$$ \boxed{ V_A = \sqrt{x_1^2 + x_2^2 + x_3^2} }$$

Angle of Attack:
$$ \alpha = \arctan \left( \frac{w}{v} \right)$$
$$ \boxed{ \alpha = \text{atan2}(x_3,x_1)} $$

Sideslip Angle:
$$  \beta = \arcsin \left( \frac{v}{Va} \right) $$
$$  \beta = \arctan \left( \frac{v}{\sqrt{u^2 + w^2}} \right) $$
$$  \beta = \arctan \left( \frac{v \cos(\alpha) }{u} \right) $$

$$ \boxed{ \beta = \arcsin \left( \frac{x_2}{V_A}  \right)} $$

Dynamic Pressure \footnote{In this model $ \rho = 1.225 [kg/m^3] $ to make indepedent from altittude and remove the former variable from the state space equations.}:

$$ Q = \frac{1}{2} \rho V_A^2 $$ 
$$ \rho = 1.225 [kg/m^3] $$

Rotation rate of the body frame in relation to the earth:

$$ \vec{\omega}_{b/e}^b = 
\begin{bmatrix}
 p \\ q \\ r
\end{bmatrix}
=
\begin{bmatrix}
 x_4 \\ x_5 \\ x_6
\end{bmatrix}
$$

$$ \vec{V}^b = 
\begin{bmatrix}
 u \\ v \\ w
\end{bmatrix}
=
\begin{bmatrix}
 x_1 \\ x_2 \\ x_3
\end{bmatrix}
$$



\section{Nondimensional Aerodynamic Force Coefficients in $F_s$}

\subsection{Lift}

$$ \boxed{ C_L = C_{L_{wb}} + C_{L_{t}}} $$

\subsubsection{Wing/body}

$$
C_{L_{wb}} = 
\begin{cases}
 n ( \alpha - \alpha_{L=0}) &  \text{if} \quad \alpha \le 14.5 \frac{\pi}{180} \\
 
 a_3 \alpha^3 + a_2 \alpha^2 + a_1 \alpha + a_0 & \text{if} \quad \alpha > 14.5 \frac{\pi}{180}
\end{cases}
$$

\begin{table}[h!t]
\begin{center}
 \begin{tabular}{cc|l}
   Property & Value    & Description \\ \hline
   
   $\alpha_{L=0}$ & $ -11.5\frac{\pi}{180} $ & Angle of Attack with zero lift. \\
   
   $n$ & 5.5 & $ \frac{\partial C_{L_{wb}}}{\partial \alpha} $ \\
   $a_3$ & -768.5 &  \\
   $a_2$ & 609.2 & Polynomial coefficients \\
   $a_1$ & -155.2 & for the stall region \\
   $a_0$ & 15.212 & \\
   
 \end{tabular}
 \caption{Lift Coefficients}
\end{center}
\end{table}


\begin{figure}[h!t]
 
\begin{tikzpicture}

\pgfmathparse{-15}
\pgfmathsetmacro{\xmin}{\pgfmathresult}
\pgfmathparse{20}
\pgfmathsetmacro{\xmax}{\pgfmathresult}
\pgfmathsetmacro{\ymin}{-.1}
\pgfmathsetmacro{\ymax}{3}

\pgfmathparse{14.5}
\pgfmathsetmacro{\xnl}{\pgfmathresult}
\pgfmathsetmacro{\n}{5.5}
\pgfmathparse{-11.5*pi/180}
\pgfmathsetmacro{\alz}{\pgfmathresult}

\pgfmathsetmacro{\at}{-768.5}
\pgfmathsetmacro{\ad}{609.2}
\pgfmathsetmacro{\au}{-155.2}
\pgfmathsetmacro{\az}{15.212}

\begin{axis}[
    axis lines = center,
    xmax=28,
    ymax=3.3,
    xlabel = \(\alpha\),
    ylabel = {\(C_\alpha\)},
    legend style={at={(axis cs:-3.5,1.5)},anchor=south east}
]
%Below the red parabola is defined
\addplot [
    domain=\xmin:\xnl, 
    samples=100, 
    color=red,
]
{\n*(x*pi/180 - \alz)};
\addlegendentry{\( n(\alpha - \alpha_{L=0}) \)}

%Here the blue parabola is defined
\addplot [
    domain=\xnl:\xmax, 
    samples=100, 
    color=blue,
    ]
    {\at*(pi/180*x)^3 + \ad*(pi/180*x)^2 + \au*(pi/180*x) + \az};
\addlegendentry {\(a_3 x^3 + a_2x^2 + a_1x + a_0 \)}

% alpha L=0
\draw[gray] (35.5,25) -- (35.5,50) node [above] {$-11.5^\circ$};

% Cl(alpha=0)
\draw[gray] (140,144)  node [left] {$ 1.10392 $} -- (160,144);

% Transition
\draw[dashed, color=blue] (295,35) node[above left] {$14.5^\circ$} -- (295,283) -- (150,283) node [left] {$2.49582$};
\draw[solid, color=blue] (295,283) [solid] circle (5);

% Max Lift
\draw[dashed, color=blue] (330,35) node[above right] {$18^\circ$} -- (330,309) -- (150,309) node [left] {$2.751$};
\draw[solid, color=blue] (330,309) [solid] circle (5);

% Slope
\draw[color=red] (200,192) -- (240,192) -- (240,230.5) node [below right] {$ 5.5 $};

\end{axis}

\end{tikzpicture}

\caption{Lift coefficient for the RCAM model}

\end{figure}

\subsubsection{Tail}

In this model, the whole tail is the elevator.

The Angle of Attack is $ \alpha_t = \alpha + \delta_t $, -- so we assume not incidence angle -- but the main wing downwash will substract some degrees ($ \varepsilon $).

We will also substract the \emph{dynamic pitch pressure}, some degrees according to the pitch rate $p$

$$ \alpha_t = \alpha - \varepsilon + \delta_t + 1.3 p \frac{l_t}{V_A} $$

$$ \boxed{$$ \alpha_t = \alpha - \varepsilon + u_2 + 1.3 x_5 \frac{l_t}{V_A} $$} $$

$$ \varepsilon = \frac{\partial \varepsilon}{\partial \alpha} ( \alpha - \alpha_{L=0} ) $$

$$ \frac{\partial \varepsilon}{\partial \alpha} = 0.25 $$
Finally,

$$ C_{L_t} = 3.1 \frac{S_t}{S} \alpha_t $$



\subsection{Drag}

Drag in the stability axes has no $ \beta $ dependence.

$$  C_D = 0.13 + 0.007(5.5 \alpha + 0.654)^2 $$


\begin{figure}[h!t]
 
 \begin{tikzpicture}
 
 \begin{axis} [
    axis lines=center,
    xmin=-18,
    xmax=28,
    ymax=0.75,
    domain=-14:20,
    legend style={at={(axis cs:-5,0.5)},anchor=south east}
    ]
    
    \addplot [color=blue,samples=100, thick] 
    {0.13 + 0.07*(5.5*x*pi/180 + 0.654)^2 };
    
    \addlegendentry { \( C_D = 0.13 + 0.07(5.5 \alpha + 0.654) \) }
  
    \draw [gray] (175,30) -- (185,30) node [right] {$0.1599$};
 \end{axis}

  
 \end{tikzpicture}

 
 \caption{Drag related to alpha}
\end{figure}

The curve never has real roots, and its minimum is located at $ -6.813^\circ $, $ C_D(\alpha=-6.81) = 0.13$.

\subsection{SideForce Coefficient}

$$ C_Y = -1.6 \beta + 0.24 \delta_R $$

$$ \boxed{ C_Y = -1.6 \beta + 0.24 u_3 } $$

That's what Mr Lum says, but I (and I'n mo one to this) think in the
stability frame must be:

$$ \boxed{ C_Y = 1.6 \beta + 0.24 u_3 } $$

I proceed in Mr Lum's way.

\subsection{The Coefficients in the Wind Frame}

$$
\vec{C}_F^s = 
\begin{bmatrix}
 f_x^s \\ f_y^s \\ f_z^s
\end{bmatrix}
=
\begin{bmatrix}
 -C_D^s \\ C_y^s \\ -C_L^s
\end{bmatrix}
=
\begin{bmatrix}
 -(0.13 + 0.07(5.5 \alpha + 0.654)^2 \\
 -1.6 \beta + 0.24 u_3 \\
 -(C_{L_wb} + C_t)
\end{bmatrix}
$$

Since all the normalization has been made around the Mean Aerodynamic Chord,

$$
\vec{C}_F^W = C_{W/S}(\beta) \cdot \vec{C}_F^S 
$$

And since,

$$
C_{W/S}(\beta) =
\begin{bmatrix}
 \cos (\beta) & \sin (\beta) & 0 \\
 -\sin(\beta) & \cos(\beta) & 0 \\
 0 & 0 & 1
\end{bmatrix}
$$

The final coefficients,

$$
\vec{C}_F^W = 
\begin{bmatrix}
 - \left[0.13 + 0.07(0.654 + 5.5 \alpha)^2 \right] \cos \beta + (0.24 u_3 -1.6 \beta) \sin \beta \\
 
 (0.24 u_3 -1.6 \beta) \cos \beta + \left[0.13 + 0.07(0.654 + 5.5 \alpha)^2 \right] \sin \beta \\
 
 -5.5(0.200713 + \alpha) - 0.763077 \left[ u_2 \frac{32.24 x_5}{V_A} + \alpha - 0.25(0.200713 + \alpha) \right]
\end{bmatrix}
$$



\section{Aerodynamic Force in $F_b$}

$$
\vec{F}_A^S =
\begin{bmatrix}
 -D \\ Y \\ -L
\end{bmatrix}^S
=
\begin{bmatrix}
 -C_D \cdot Q \cdot S \\
  C_Y \cdot Q \cdot S \\
 -C_L \cdot Q \cdot S
\end{bmatrix}^S
$$

$$
\vec{F}_A^b = C_{b/s}(\alpha) \vec{F}_A^S
$$

$$
C_{b/s}(\alpha) = 
\begin{bmatrix}
 \cos \alpha & 0 & -\sin \alpha \\
 0 & 1 & 0 \\
 \sin \alpha & 0 & \cos \alpha
\end{bmatrix}
$$

\section{Nondimensional aerodynamic Moment Coefficients abot AC in $F_b$}

Being $l$, $m$ and $n$ roll, pitch and yaw moments ($x$,$y$,$z$),
$$
\vec{C}_{M_{ac}}^b =
\begin{bmatrix}
 C_{l_{ac}} \\
 C_{m_{ac}} \\
 C_{n_{ac}} \\
\end{bmatrix}^b
= \vec{\eta} + \frac{\partial C_M}{\partial x} \vec{\omega}_{b/e}^b + \frac{\partial C_M}{\partial u} 
\begin{bmatrix}
 u_1 \\ u_2 \\ u_3
\end{bmatrix}
$$

$$ \eta = \begin{bmatrix}
           -1.4 \beta \\ \\
           -0.59 \beta -3.1 \frac{S_t l_t}{S \bar{c}} (\alpha - \varepsilon) \\
           \\
           \left( 1 - \alpha \frac{180}{15 \pi} \right) \beta
          \end{bmatrix}
$$



$ eta $ can be interpretred in sequential order as:

\begin{enumerate}
 \item Sideslip banking.
 \item Combined (fusselage and wings vs tail) pitch Moment.
 \item Fusselage, swept back wings and tail longitudinal stability.
\end{enumerate}

The variaton of the moments with the angular velocity

$$
\frac{\partial C_M}{\partial x} = 
\begin{bmatrix}
 \frac{\partial C_l}{\partial p} & \frac{\partial C_l}{\partial q} & \frac{\partial C_l}{\partial r} \\ \\
 \frac{\partial C_m}{\partial p} & \frac{\partial C_m}{\partial q} & \frac{\partial C_m}{\partial r} \\ \\
 \frac{\partial C_n}{\partial p} & \frac{\partial C_n}{\partial q} & \frac{\partial C_n}{\partial r} 
\end{bmatrix}
$$

Where $ \frac{\partial C_l}{\partial p} = -11 $ is the opposition to banking because of the yoke / stick and $ \frac{\partial C_l}{\partial r} = 5 $ takes into account that continously yawing increases the amount of wind in the opposite wing increasing the roll.

When pitching there is a wind blowing  down to the wing.

The yawing moment $ \frac{\partial C_n}{\partial p} = 1.7 $ means that rolling substract lift in one of the wings and since there is a lost of wind, there is also a lost in lift and a lost in drag.

$$
\frac{\partial C_M}{\partial x} = 
\begin{bmatrix}
 -11 & 0 & 5 \\
 0 & -4.03 \frac{S_t l_t^2}{S \bar{c}} & 0 \\
 1.7 & 0 & -1.5
\end{bmatrix}
$$

$$
\frac{\partial C_M}{\partial u} = 
\begin{bmatrix}
 0.6 & 0   & -0.22 \\
 0 & -3.1 \frac{S_t l_t}{S \bar{c}} & 0 \\
 0 & 0 & -0.63
\end{bmatrix}
$$


$ \frac{\partial C_l}{\partial \delta_{tail}} = -0.22 $ means that deflecting the rudder, with an
aerodynamic center above CoG, tries to restore the banking.

$ \frac{\partial C_n}{\partial \delta_{ail}} = 0 $ states that we are not taking into account adverse yaw.

\section{Aerodynamic Moments about AC in $F_b$}

$$ \vec{M}_{A_{ac}}^b = \vec{C}_{M_{ac}}^b \cdot q \cdot S \cdot \bar{c} $$

\section{Aerodynamic Moments about CoG in $F_b$}


$$
\vec{M}_{A_{cg}}^b = \vec{M}_{A_{ac}} + \vec{F}_A^b \times (\vec{r}_{cg} - \vec{r}_{ac})
$$

\begin{table}[h!t]
 \begin{center}
 \begin{tabular}{|c|c|c|}
  \hline \hline
  Coordinate & Bounds & Nominal \\ \hline \hline 
  & & \\
  $X_{cg}$ & $0.15 \bar{c} < X_{cg} < 0.31 \bar{c} $ & $ 0.23 \bar{c} $ \\
  & & \\
  $Y_{cg}$ & $ -0.03 \bar{c} < Y_{cg} < 0.03 \bar{c} $ & $0.00  \bar{c} $\\
  & & \\
  $Z_{cg}$ & $ 0.00 \bar{c}< Z_{cg} < 0.21 \bar{c} $ & $ 0.10 \bar{c} $ \\
  & & \\
  \hline
 \end{tabular}

 \caption{CG limits.}
 
 \end{center}
\end{table}

$$
\vec{r}_{ac} =
\begin{bmatrix}
 X_{ac} \\ Y_{ac} \\ Z_{ac} 
\end{bmatrix}
=
\begin{bmatrix}
 0.12 \bar{c} \\ 0.00 \\ 0.00 
\end{bmatrix}
$$

Let's transfer the moment to the CoG \footnote{In the RCAM model, forces are modeled in $F_s$ and moments in $F_b$.}

$$
\begin{bmatrix}
 C_{l_{cg}} \\  C_{m_{cg}} \\  C_{n_{cg}} 
\end{bmatrix}
=
\begin{bmatrix}
 C_{l} \\  C_{m} \\  C_{n} 
\end{bmatrix}
+ \frac{1}{\bar{c}}
\begin{bmatrix}
 X_{cg} \\  Y_{cg} \\ Z_{cg} 
\end{bmatrix}
\times \left( 
R_{B/S}
\begin{bmatrix}
 -C_{D} \\  C_{Y} \\ -C_{L} 
\end{bmatrix}
\right)
$$

And 
$$
R_{B/S} = 
\begin{bmatrix}
 \cos \alpha & 0 & -\sin \alpha \\
 0 & 1 & 0 \\
 \sin \alpha & 0 & \cos \alpha
\end{bmatrix}
$$

The total Moment

$$
\vec{M}_{A_{cg}}^b =
\begin{bmatrix}
 C_{l_{cg}} \cdot \frac{1}{2} \rho \cdot V_A^2 \cdot S \cdot \bar{c} \\
 \\
 C_{m_{cg}} \cdot \frac{1}{2} \rho \cdot V_A^2 \cdot S \cdot \bar{c}\\ \\
 C_{n_{cg}} \cdot \frac{1}{2} \rho \cdot V_A^2 \cdot S \cdot \bar{c}
\end{bmatrix}
$$

\section{Propulsion Effects}

$$ F_i = \delta_{Thi} \cdot m \cdot g  $$

$$ F_i = 
\begin{cases}
F_1 = \delta_{Th1} \cdot m \cdot g  \\
F_2 = \delta_{Th2} \cdot m \cdot g   
\end{cases}
$$

Above limit is
$$ u_4, u_5 \le 10 \frac{\pi}{180} $$

$$ \frac{F_{1max} + F_{2max}}{mg} = 0.35 $$

which is a very normal thrust / weight ratio.

Force:

$$ \vec{F}_{E_i}^b =
\begin{bmatrix}
 F_i \\ 0 \\ 0
\end{bmatrix}
$$

$$ \vec{F}_E^b = \vec{F}_{E_1}^b + \vec{F}_{E_2}^b $$

Moments:

$$ \vec{\mu}_i^b =
\begin{bmatrix}
  X_{cg} - X_{APT_1} \\
  Y_{APT_1} - Y_{cg} \\
  Z_{cg} - Z_{APT_1} 
\end{bmatrix}
$$

$$ \vec{M}_{E_{cgi}}^b = \vec{\mu}_i^b \times \vec{F}_{E_i}^b $$
$$ \vec{M}_{E_{cg}}^b = \vec{M}_{E_{cg1}}^b + \vec{M}_{E_{cg2}}^b$$

\begin{table}[h!t]
 \begin{center}
 \begin{tabular}{|cc|}
 
 Coordinate & Application[m] \\
 
 $X_{apt1}$ & 0.0   \\
 $Y_{apt1}$ & -7.94 \\
 $Z_{apt1}$ & -1.9  \\
 
 $X_{apt2}$ & 0.0   \\
 $Y_{apt2}$ & 7.94 \\
 $Z_{apt2}$ & -1.9  \\
  
 \end{tabular}
 \end{center}
 \caption{Engine Parameters.}
\end{table}

\section{Explicit First Order Equations}

$$
\dot{\vec{x}} = f(\vec{x}, \vec{u}) = 
\begin{bmatrix}
 \frac{1}{m} \vec{F}^b - \vec{\omega}_{b/e}^b \times \vec{V}^b \\
 I_b^{-1} \left( \vec{M}^b - \vec{\omega}_{b/e}^b \times I_b \vec{\omega}_{b/e}^b \right) \\
 H(\Phi) \vec{\omega}_{b/e}^b
\end{bmatrix}
$$

$$
\begin{bmatrix}
  \dot{x_1} \\ \dot{x_2} \\ \dot{x_3} 
\end{bmatrix}
 = 
 \begin{bmatrix}
  \dot{u} \\ \dot{v} \\ \dot{w} 
\end{bmatrix}
 = 
 \frac{1}{m} \vec{F}^b - \vec{\omega}_{b/e}^b \times \vec{V}^b \\
$$


$$
\begin{bmatrix}
  \dot{x_4} \\ \dot{x_5} \\ \dot{x_6} 
\end{bmatrix}
 = 
 \begin{bmatrix}
  \dot{p} \\ \dot{q} \\ \dot{r} 
\end{bmatrix}
 = 
I_b^{-1} \left( \vec{M}^b - \vec{\omega}_{b/e}^b \times I_b \vec{\omega}_{b/e}^b \right) \\
$$

$$
\begin{bmatrix}
  \dot{x_7} \\ \dot{x_8} \\ \dot{x_9} 
\end{bmatrix}
 = 
\begin{bmatrix}
  \dot{\phi} \\ \dot{\theta} \\ \dot{\psi} 
\end{bmatrix}
 \begin{bmatrix}
  1 & \sin \phi \tan \theta & \cos \phi \tan \theta \\
  0 & \cos \phi & -\sin \phi \\
  0 & \frac{\sin \phi}{\cos \theta} & \frac{cos \phi}{\cos \theta} \\
 \end{bmatrix}
 \cdot
 \begin{bmatrix}
  p \\ q \\ r 
\end{bmatrix}^b
$$
\end{document}
