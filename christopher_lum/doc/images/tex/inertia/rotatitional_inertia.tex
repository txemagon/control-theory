\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}

%opening
\title{Aircraft Rotational Inertia Characterization}
\author{José María González}

\begin{document}

\maketitle

\section{Rotational Inertia}


\subsection{The Equations for a System of Particles}

For a system of particles, given a reference point R (that might not be the center of gravity).

$$ \vec{r}_i = \vec{R} + ( \vec{r}_{iR} - \vec{R} ) $$

One particle keeps constant:

$$
\vec{p}_i = m_i \vec{v}_i 
$$

$$
\vec{L}_i = \vec{r}_i \times \left( \frac{d}{dt} [\vec{r}_i] \right)
$$

Referenced to $\vec{R}$:

$$
\vec{L}_i = m_i \left( \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right) \times \left( \frac{d}{dt} \left[ \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right] \right)
$$

Accounting all particles:

$$
\vec{L} =  \sum m_i \left( \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right) \times \left( \frac{d}{dt} \left[ \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right] \right)
$$

$$
\vec{L} = \sum \vec{R} \times \left( \frac{d}{dt} \left[ \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right] \right)  +  \sum m_i \left( \vec{r}_{iR} - \vec{R} \right) \times \left( \frac{d}{dt} \left[ \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right] \right)
$$


$$
\vec{L} = M \vec{R} \times \vec{V} +  \vec{R} \times \sum m_i \left( \frac{d}{dt} \left[ ( \vec{r}_{iR} - \vec{R} ) \right] \right)  +  \sum m_i \left( \vec{r}_{iR} - \vec{R} \right) \times \left( \frac{d}{dt} \left[ \vec{R} + ( \vec{r}_{iR} - \vec{R} ) \right] \right)
$$

For the center of gravity:


$$
\vec{L} = M \vec{R} \times \vec{V}  + \sum m_i  \left( \vec{r}_{iR} - \vec{R} \right) \times \frac{d}{dt} \left[ ( \vec{r}_{iR} - \vec{R} ) \right] 
$$


For a \emph{Rigid System of Particles}:

$$ \frac{d}{dt} \left[ \vec{r}_i \right] = \vec{V} + \vec{\omega} \times \left( \vec{r}_i - \vec{R} \right) $$

And

$$
\vec{L} = M\vec{R} \times \vec{V} + \sum m_i \left( \vec{r}_i - \vec{R} \right) \times \left[ \vec{\omega} \times \left( \vec{r}_i - \vec{R} \right) \right]
$$

\subsection{Inertia as a Tensor}

$$
\vec{r} \times \vec{a} = 
\begin{vmatrix}
 
 \vec{i} & \vec{j} & \vec{k} \\
 r_x & r_y & r_z \\
 a_x & a_y & a_z \\
\end{vmatrix}
= \left[ r_ya_z - r_za_y\right] \vec{i} +
\left[ r_za_x - r_xa_z \right] \vec{j} +
\left[ r_xa_y - r_ya_x \right] \vec{k}
$$

That can be written as:

$$
\vec{r} \times \vec{a} = 
\begin{bmatrix}
 
 0 & -r_z & r_y \\
 r_z & 0 & -r_x \\
 -r_y & r_x & 0 \\
\end{bmatrix}
\vec{a}
= R \vec{a}
$$

We can check that:

$$ - R \cdot R = R \cdot R^T $$


So

$$ 
\vec{r} \times ( \vec{\omega} \times \vec{r} ) =
- \vec{r} \times ( \vec{r} \times \vec{\omega}  ) =
- R \cdot R \vec{\omega} = R \cdot R^T \vec{\omega} =
\begin{bmatrix}
 r_y^2 + r_z^2 & -r_xr_y & -r_xr_z \\
 -r_xr_y & r_z^2 + r_x^2 & -r_yr_z \\
 -r_xr_z & -r_yr_z & r_x^2 + r_y^2 \\
\end{bmatrix}
$$

The matrix $R$ can be expressed using the outer product as:

$$
R = (\vec{r} \cdot \vec{r}) I - \vec{r} \otimes \vec{r} 
$$

Or with the Kronecker delta:
$$
R_i = |r|^2 \cdot \delta_{ij} - r_ir_j 
$$

Or in the diadic form:

$$
R = r^2 - \vec{r}\vec{r}
$$

Notice the missing dot indicating diadic product.

\subsection{Tensor of Inertia}

$$ 
\vec{L} = \int  \vec{r} \times ( \vec{\omega} \times \vec{r} ) dm =
\int
\begin{bmatrix}
 r_y^2 + r_z^2 & -r_xr_y & -r_xr_z \\
 -r_xr_y & r_z^2 + r_x^2 & -r_yr_z \\
 -r_xr_z &- r_yr_z & r_x^2 + r_y^2 \\ 
\end{bmatrix}
\vec{\omega} dm
$$

$$
\vec{L} = 
\begin{bmatrix}
 \int r_y^2 + r_z^2 dm & -\int r_xr_y dm & -\int r_xr_z dm \\
 -\int r_xr_y dm & \int r_z^2 + r_x^2 dm & -\int r_yr_z dm \\
 -\int r_xr_z dm & -\int r_yr_z dm & \int r_x^2 + r_y^2 dm \\ 
\end{bmatrix}
\vec{\omega} =
I\vec{\omega}
$$

\section{Theorems}


\subsection{Plane Figure}

For every 2D figure:

$$ I_{zz} = I_{xx} + I_{yy} $$

\subsection{Cylinder out of a Plane Figure}

If $ I_{xx} $ and $ I_{yy} $ have been calculated for a flat figure accounting
the density $\rho$ in $\frac{kg}{m^2}$. What would be the inertia tensor if
a prismatic cylinder is made out of it, raising its height a distance $D$ over the $xy$ plane?

Let be a flat figure with:

$$ I^1_{zz} = \rho \frac{V}{S} \int (x^2 + y^2) dx dy$$

If such a figure is not considered flat, but a cylinderm then

$$ I_{zz} = \rho \int (x^2 + y^2) dx dy dz$$

Operating, we will reach:

$$ \boxed{I_{zz} = I^1_{zz}} $$

For pretty much the same reason:

$$ \boxed{ P_{xy} = P_{xy}^1 } $$

But what about $ I_{xx}$?

$$ I_{yy} = \rho \int x^2 dx dy dz + \rho \int z^2 dx dy dz  $$

Referred to the flat figure:

$$ I_{yy} = I^1_{yy} + \rho \int z^2 dx dy dz = I^1_{xx} + \rho S \int z^2 dz  $$

Assumming the distance $D$ looms entirely over the $xy$ plane:

$$ \boxed{ I_{yy} = I^1_{yy} + M \frac{D^2}{3} } $$

In case such distance is halved symmetrically around the $xy$ plane:

$$ \boxed{ I_{yy} = I^1_{yy} + M \frac{D^2}{12} } $$

And lastly,

$P_{xz}$ and $P_{yz}$ can be calculated:

$$ P_{xz} = \rho \int x z dx dy dz $$

Recall that the limits of integration for $z$ don't rely on $x$, so:

$$ P_{xz} = \rho \int \left( \int x dx dy \right) z dz $$

That's related to center of area/gravity of the section.

$$ P_{xz} = \rho X_G S\int z dz $$

In a non symetrical case that would be:

$$ \boxed{ P_{xz} = M X_G \frac{D}{2}} $$

Or $0$ in the symmetrical one.

\subsection{Summary}

Plane figure:

$$
I = 
\begin{pmatrix}
 I_{xx} & -P_{xy} & 0 \\
 -P_{xy} & I_{yy} & 0 \\
 0 & 0 & I_{xx} + I_{yy}
\end{pmatrix}
$$

For a normal extrusion of the figure in the $z$ axis of size $D$:

$$
I = 
\begin{pmatrix}
 I_{xx} + \frac{1}{3} M D^2  & -P_{xy} & -\frac{1}{2} M X_G D \\
 -P_{xy} & I_{yy} +\frac{1}{3} M D^2  & -\frac{1}{2} M Y_G D \\
 -\frac{1}{2} M X_G D & -\frac{1}{2} M Y_G D & I_{xx} + I_{yy}
\end{pmatrix}
$$

If the extrusion were to be symmetrical around $xy$ plane:

$$
I = 
\begin{pmatrix}
 I_{xx} + \frac{1}{12} M D^2  & -P_{xy} & 0 \\
 -P_{xy} & I_{yy} +\frac{1}{12} M D^2  & 0 \\
 0 & 0 & I_{xx} + I_{yy}
\end{pmatrix}
$$


\subsection{Steiner Theorem}

Let,

$$ \vec{R} = x\vec{i} + y\vec{j} + z\vec{k} $$

The new moment of Inertia, $J$, related to the old one, $I$:

$$ J = I + mR^2 $$

\subsection{Parallel Axes Theorem}

And for the whole tensor,the new tensor of inertia, $J$, related to the old one, $I$:


$$ J_{ij} = I_{ij} + m(|R|^2 \delta_{ij} - R_i R_j )  $$

Or

$$ J = I + m(\vec{R} \cdot \vec{R} E_3 - \vec{R} \otimes \vec{R} ) $$

Which in both cases end up being:

$$
J = 
\begin{bmatrix}
 I_{xx} & -P_{xy} & -P_{xz} \\
 -P_{xy} & I_{yy} & -P_{yz} \\
 -P_{xz} & -P_{yz} & I_{zz} \\
\end{bmatrix}
+ m \left[ (x^2 + y^2 + z^2) 
\begin{bmatrix}
 1 & 0 & 0 \\    
 0 & 1 & 0 \\
 0 & 0 & 1 \\
\end{bmatrix}
-
\begin{bmatrix}
 x^2 & xy & xz \\    
 xy & y^2 & yz \\
 xz & yz & y^2 \\
\end{bmatrix}
\right]$$

Or simply:
$$
J = 
\begin{bmatrix}
 I_{xx} & -P_{xy} & -P_{xz} \\
 -P_{xy} & I_{yy} & -P_{yz} \\
 -P_{xz} & -P_{yz} & I_{zz} \\
\end{bmatrix}
+ m
\begin{bmatrix}
 y^2 + z^2 & -xy & -xz \\
 -xy & z^2 + x^2 & -yz \\
 -xz & -yz & x^2 + y^2 \\ 
\end{bmatrix}
$$

\subsection{Moment of Inertia around an arbitrary Axis}

If the tensor of inertia, $I_O$,  is known for a point,$O$, of a given axis, $\Delta$, whose direction is given by $ \vec{n} $

The perpendicular distance,$a$, from a mass, $dm$, to the axis $n$ can be calculated when known the vector,$\vec{r}$ from $O$ to $dm$ as:

$$ a = r^2 - \vec{r} \cdot \vec{n} $$

And so;

$$ \boxed{I_\Delta = \vec{n}^T \cdot I_O \cdot \vec{n}} $$

Proof using diadic algebra:

\begin{equation}
\begin{split}
 \vec{n} \cdot I_O \cdot \vec{n} &= \vec{n} \left(\cdot \int U \int r^2 dm - \int \vec{r} \vec{r} dm) \cdot \right) \vec{n} \\
 &=  \vec{n} U \vec{n} \int r^2 dm - \vec{n} \left( \int \vec{r} \vec{r} dm \right) \cdot  \vec{n} \\
 &= \int r^2 dm - \int ( \vec{r} \cdot \vec{n})^2 dm\\
 &= \int r^2 - ( \vec{r} \cdot \vec{n})^2 dm \\
 &= \int a^2 dm \\
 &= I_\Delta
\end{split}
\end{equation}

If there aren't products of inertia:

$$ \boxed{ I_\Delta = I_{xx} \cos^2 \alpha + I_{yy} \cos^2 \beta + I_{zz} \cos^2 \gamma }$$

\section{Some tensors of Inertia}

Flat righttriangle of dimensions $l_x$, $l_y$. Inertia referred to the intersection of the legs:

$$
I = \frac{M}{6} 
\begin{bmatrix}
 l_y^2 & -\frac{1}{2} l_x l_y & 0 \\
  -\frac{1}{2} l_x l_y & l_x^2 & 0 \\
  0 & 0 & l_x^2 + l_y^2
\end{bmatrix}
$$

Triangular prism extruded symmetrically $l_z$ from previous flat figure:

$$
I = \frac{M}{6} 
\begin{bmatrix}
 l_y^2 + \frac{1}{2} l_z^2 & -\frac{1}{2} l_x l_y & 0 \\
  -\frac{1}{2} l_x l_y & l_x^2 + \frac{1}{2} l_z^2 & 0 \\
  0 & 0 & l_x^2 + l_y^2
\end{bmatrix}
$$


Rectangular prism around center of gravity. Dimensions $l_x$, $l_y$, $l_z$


$$
I = \frac{M}{12} 
\begin{bmatrix}
 l_y^2 + l_z^2 & 0 & 0 \\
  0 & l_x^2 + l_z^2 & 0 \\
  0 & 0 & l_x^2 + l_y^2
\end{bmatrix}
$$


\section{Aircraft Characterization}

\subsection{Static Characterization}

We will be using a circular scale, radius $r$, suspended by $N$ wires vertically separated a distance $L$ from the hanger.

We can stablish that:

$$ \omega_n = \sqrt{\frac{r}{L} \frac{mg}{I_{zz}}}$$

Conversely,

$$ I = \frac{1}{4 \pi^2} \frac{r}{L} mg \cdot T $$

being $T$ the oscillation period.

If we tilt the aircraft on the scale $\alpha$ degrees, and as a result of that we have to
move the plane forward a distance $\delta$ to have all the wires balanced again, then,

$$ Y_{CG} = r \tan \left[ \arccos \left( \frac{\delta}{r} \right) - \alpha \right] $$

And measureing the new $I_{zz} = I_\Delta$, we'd be able to estimate $I_{xx}$ or $I_{yy}$:

$$ I_{xx} = \frac{I_\Delta - I_{zz} \cos^2 \alpha}{\sin^2 \alpha} $$

Or tilting in the perpendicular axis,
$$ I_{yy} = \frac{I_\Delta - I_{zz} \cos^2 \alpha}{\sin^2 \alpha} $$

\subsection{Dynamic Characterization}

There's also a method published by NASA on a separated pdf to characterize moments of inertia and dynamic coefficients through flight maneuvers and data analysis.

\end{document}
