# Flight Mechanics 2

In this document we summarize, according to Christopher Lum's online lectures a study of
the RCAM mathematical model from the aerodynamic characterization to the modal analysis.

## Wind Tunnel Coefficients

### Presentation

Forces:

```math
C_L = \frac{L}{q \cdot S}  \\
C_D = \frac{D}{q \cdot S}  \\
C_Y = \frac{SF}{q\cdot S}
```

Moments:

```math
C_l = \frac{RM}{q \cdot S \cdot b_{ref}} \\
C_m = \frac{PM}{q \cdot S \cdot \bar{c}} \\
C_n = \frac{YM}{q \cdot S \cdot b_{ref}} 
```

### Addition

Separating Wind and Tail coefficients and putting them all together:

```math
C_L = \frac{C_W \cdot S_W  + C_t \cdot S_t }{S_W} = C_w + C_t \frac{S_t}{S_W}
```


### Rotation

To rotate coefficients:

```math
C_M^b =
\begin{bmatrix}
C_l \cdot b_{ref} \\
C_m \cdot \bar{c} \\
C_n \cdot b_{ref} \\
\end{bmatrix} ^W
\cdot C_{b/W} \cdot
\begin{bmatrix}
\frac{1}{b_{ref}} &  \frac{1}{\bar{c}}  & \frac{1}{b_{ref}}
\end{bmatrix}
```

## RCAM. An aircraft model

Found in this [pdf](./research_civil_aircraft_model.pdf).

## Similarity Transformation of a Linear Dynamic System

Given the system
```math
\dot{\bar{x}}= A \bar{x} + B \bar{u} \\
\bar{y} = C \bar{x} + D \bar{u}
```

and a transformation,

```math
\vec{x} = T \bar{z}
```

then

```math
T \dot{\bar{z}}= A T \bar{z} + B \bar{u} \\
\bar{y} = C \bar{T \bar{z}} + D \bar{u} \\
```

```math
\dot{\bar{z}}= T^{-1} A T \bar{z} + T^{-1} B \bar{u} \\
\bar{y} = C T \bar{z} + D \bar{u} \\
```

For short,
```math
\dot{\bar{z}}= \tilde{A} \bar{z} + \tilde{B} \bar{u} \\
\bar{y} = \tilde{C}\bar{z} + \tilde{D} \bar{u} \\
```

The transfer functions,
```math
G(s) = C(sI-A)^{-1}B+D
\tilde{G}(s) = \tilde{C}(sI-\tilde{A})^{-1}\tilde{B}+\tilde{D}
```

Are they equal?

```math
\tilde{G}(s) = CT(sI-T^{-1}AT)^{-1}T^{-1}B + D \\
\tilde{G}(s) = CT\left[ T^{-1}(sI-A)T \right ]^{-1}T^{-1}B + D \\
\tilde{G}(s) = CT\left[ T^{-1} \left[T^{-1} (sI-A) \right ]^{-1} \right ]T^{-1}B + D \\
\tilde{G}(s) = CT\left[ T^{-1} \left[ (sI-A)^{-1}T \right ] \right ]T^{-1}B + D \\
\tilde{G}(s) = C(sI-A)^{-1}B + D \\
```

On a similarity transformation:
```math
\tilde{A} = T^{-1}AT  \\
```

1. Only internal state differs.
1. Same eigenvalues and poles.

To make a linear transformation over nonlinear equations:

```math
\bar{x} = \begin{bmatrix}
u\\ 
v\\ 
w\\ 
p\\ 
q\\ 
r\\ 
\phi\\ 
\theta\\ 
\psi
\end{bmatrix}
\\
\bar{z} = \begin{bmatrix}
V_a\\ 
\alpha\\ 
\beta\\ 
p\\ 
q\\ 
r\\ 
\phi\\ 
\theta\\ 
\psi
\end{bmatrix}
```

where,

```math
V_a = (u^2 + v^2 + w^2)^{1/2} \\
\\
\alpha = \arctan \left( \frac{w}{u}  \right ) \\
\\
\beta = \arcsin \left( \frac{v}{(u^2 + v^2 + w^2)^{1/2}} \right )

```
 
 The linearization,
 
```math
\Delta V_a = \frac{\partial V_a(u_o,v_o,w_o)}{\partial u} \Delta u + \frac{\partial V_a(u_o,v_o,w_o)}{\partial v} \Delta v + \frac{\partial V_a(u_o,v_o,w_o)}{\partial w} \Delta w \\
\\
\Delta \alpha = \frac{\partial \alpha(u_o,v_o,w_o)}{\partial u} \Delta u + \frac{\partial \alpha(u_o,v_o,w_o)}{\partial v} \Delta v + \frac{\partial \alpha(u_o,v_o,w_o)}{\partial w} \Delta w 
\\
\\
\Delta \beta = \frac{\partial \beta(u_o,v_o,w_o)}{\partial u} \Delta u + \frac{\partial \beta(u_o,v_o,w_o)}{\partial v} \Delta v + \frac{\partial \beta(u_o,v_o,w_o)}{\partial w} 
```

Some magic after,

```math
\Delta V_a = \frac{u_o}{\sqrt{u_o^2 + v_o^2 + w_o^2}} \Delta u + \frac{v_o}{\sqrt{u_o^2 + v_o^2 + w_o^2}} \Delta v + \frac{w_o}{\sqrt{u_o^2 + v_o^2 + w_o^2}} \Delta w \\
\\
\Delta \alpha = \frac{- w_o}{u_o^2 + w_o^2} \Delta u + 0 \Delta v + \frac{u_o}{u_o^2 + w_o^2} \Delta w 
\\
\\
\Delta \beta = \frac{-u_o v_o}{\sqrt{\frac{u_o^2 + w_o^2}{u_o^2 + v_o^2 + w_o^2}}(u_o^2 + v_o^2 + w_o^2)} \Delta u + \frac{\sqrt{\frac{u_o^2 + w_o^2}{u_o^2 + v_o^2 + w_o^2}}}{\sqrt{u_o^2 + v_o^2 + w_o^2}} \Delta v + \frac{-v_ow_o}{\sqrt{\frac{u_o^2 + w_o^2}{u_o^2 + v_o^2 + w_o^2}}(u_o^2 + v_o^2 + w_o^2)} \Delta w 
```


The similarity transformation can also be used to alter state variable order.

Lastly, the system can be diagonalized by this method.

```matlab
T = eigenvectors(A);
```

Modal decomposition of the system.

## Longitudinal and Lateral/Directional Models

- Longitudinal States => _u_ and _w_ are coupled. Also _θ_ and _q_. Plane of body symmetry xz.

```math
\bar{x}_{long} = \begin{bmatrix}
u\\ 
w\\ 
q\\ 
\theta
\end{bmatrix}
=
\begin{bmatrix}
x_1\\ 
x_3\\ 
x_5\\ 
x_8
\end{bmatrix}
```

- Lateral states

```math
\bar{x}_{long} = \begin{bmatrix}
v\\ 
p\\ 
r\\ 
\phi
\end{bmatrix}
=
\begin{bmatrix}
x_2\\ 
x_4\\ 
x_6\\ 
x_7
\end{bmatrix}
```


Reordering the states

```math
\bar{z} = \begin{bmatrix}

\bar{x}_{long}\\ 
\bar{x}_{lat} \\ 
\psi
\end{bmatrix}
=
\begin{bmatrix}
u\\ 
w\\ 
q\\ 
\theta \\
\\
v\\ 
p\\ 
r\\ 
\phi\\
\\
\psi
\end{bmatrix}
=
\begin{bmatrix}
x_1\\ 
x_3\\ 
x_5\\ 
x_8 \\
\\
x_2\\ 
x_4\\ 
x_6\\ 
x_7\\
\\
x_9
\end{bmatrix}
=
\begin{bmatrix}
z_1\\ 
z_2\\ 
z_3\\ 
z_4 \\
\\
z_5\\ 
z_6\\ 
z_7\\ 
z_8\\
\\
z_9
\end{bmatrix}
```

Through a similarity transformation:

```math
\begin{bmatrix}
z_1\\ 
z_2\\ 
z_3\\ 
z_4 \\
\\
z_5\\ 
z_6\\ 
z_7\\ 
z_8\\
\\
z_9
\end{bmatrix}
=
\begin{bmatrix}
x_1\\ 
x_3\\ 
x_5\\ 
x_8 \\
\\
x_2\\ 
x_4\\ 
x_6\\ 
x_7\\
\\
x_9
\end{bmatrix}
=\begin{bmatrix}
 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\ 
 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\ 
 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\ 
 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\ 
\\
 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\ 
 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\ 
 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\ 
 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\ 
\\
 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 
\end{bmatrix}
\begin{bmatrix}
x_1\\ 
x_2\\ 
x_3\\ 
x_4 \\
\\
x_5\\ 
x_6\\ 
x_7\\ 
x_8\\
\\
x_9
\end{bmatrix}
```

To sum up:

```math
\bar{z} = P \bar{x} \\
\bar{x} = P^{-1} \bar{z} = T \bar{z}\\
```

Now we have an internal state around _z_, and given an input _u_, there'll be an output _z_, such that:

```math
\dot{\bar{z}} = \tilde{A} \bar{z} + \tilde{B} \bar{u} \\
\\
\tilde{A} = T^{-1} \cdot A \cdot T \\
\tilde{B} = T^{-1} \cdot B
```

And now we get a decoupled system where _Along = A(1:4, 1:4)_ and _Blong = B(1:4,:)_, so

```math
\bar{x}_{long} = A_{long} \cdot \bar{x}_{long} + B_{long} \cdot \bar{u}
```

## Modal Analisys

1. Phugoid
1. Short Period
1. Dutch Roll
1. Roll Subsidence
1. Spiral Modes


### Longitudinal Modes

```math
\bar{x}_{lon} =
\begin{bmatrix}
u\\ 
w\\ 
q\\ 
\theta
\end{bmatrix}
=
\begin{bmatrix}
x_1\\ 
x_3\\ 
x_5\\ 
x_8
\end{bmatrix}
```

Looking at the eigen values and the eigenvectors:

```matlab
T = eigenvectors(Along)
```

```math
eig(A_{long})=\begin{bmatrix}
-0.909 + 1.650i \\ 
-0.909 - 1.650i \\ 
-0.014 + 0.134i \\ 
-0.014 - 0.134i
\end{bmatrix}
```

Being _Vi_ the eigenvector associated with eigenvalue i, recalling that in the linearization around the
trimming point we are always talking about increments, we can say:

```math
\bar{x}_{long}(t) = \\
\bar{v}_1 z_{1o} e^{(-0.909+1.650i)t} + \\
\bar{v}_2 z_{2o} e^{(-0.909-1.650i)t} + \\
\bar{v}_3 z_{3o} e^{(-0.014+0.135i)t} + \\
\bar{v}_4 z_{4o} e^{(-0.014-0.135i)t}
```

where

```math
\bar{v}_1 =
\begin{bmatrix}
0.0154-0.0122i\\ 
0.9995\\ 
-0.0024+0.0201i\\ 
-0.0100-0.0040i
\end{bmatrix}
\quad

\bar{v}_3 =
\begin{bmatrix}
-0.9957\\ 
0.0919-0.0015i\\ 
-0.0019-0.0002i\\ 
0.0029-0.0137i
\end{bmatrix}
```

The damping ratios:

```math

\\
\lambda = a + ib
\\
\zeta = \frac{a}{\sqrt{a^2 + b^2}} \\
\\
\zeta_1 = 0.4825
\zeta_2 = 0.1039
```

The first pair of eigenvalues is quick and is very damped. It's called **short period mode**.
The second pair of eigenvalues is slow and not damped. It's called **phugoid mode**.

From wikipedia we know:

> The phugoid has a nearly constant angle of attack but varying pitch, caused by a repeated exchange of airspeed and altitude. It can be excited by an elevator singlet (a short, sharp deflection followed by a return to the centered position) resulting in a pitch increase with no change in trim from the cruise condition. As speed decays, the nose drops below the horizon. Speed increases, and the nose climbs above the horizon. Periods can vary from under 30 seconds for light aircraft to minutes for larger aircraft. Microlight aircraft typically show a phugoid period of 15–25 seconds, and it has been suggested[by whom?] that birds and model airplanes show convergence between the phugoid and short period modes. 


To evaluate _Xlong(t)_ for the first pair of eigenvectors, associated to the fast and highly damped eigenvalue  0.909 + i1.650 (short period)

Lets use an initial condition:

```math
\Delta \bar{z} =
\begin{bmatrix}
1\\ 
1\\ 
0\\ 
0
\end{bmatrix}
\implies
\Delta \bar{x} = T \Delta \bar{z} \\
\Delta \bar{x} =
\begin{bmatrix}
   0.0154 + 0.0122i &  0.0154 - 0.0122i & -0.9956 + 0.0000i & -0.9956 + 0.0000i \\
   0.9995 + 0.0000i &  0.9995 + 0.0000i &  0.0921 + 0.0015i &  0.0921 - 0.0015i \\
  -0.0024 + 0.0201i & -0.0024 - 0.0201i & -0.0019 + 0.0002i & -0.0019 - 0.0002i \\
   0.0100 - 0.0040i &  0.0100 + 0.0040i &  0.0029 + 0.0137i &  0.0029 - 0.0137i
\end{bmatrix}
\begin{bmatrix}
1\\ 
1\\ 
0\\ 
0
\end{bmatrix}

\\
 
\\
 
\Delta \bar{x} =
\begin{bmatrix}
    0.0308 \\
    1.9991 \\
   -0.0048 \\
    0.0199
\end{bmatrix}
```

For the Phugoid

```math
\Delta \bar{z} =
\begin{bmatrix}
0\\ 
0\\ 
1\\ 
1
\end{bmatrix}
\\
\Delta \bar{x} =
\begin{bmatrix}
   -1.9913 \\
    0.1843 \\
   -0.0038 \\
    0.0058
\end{bmatrix}
```

### Lateral Modes

```math
\bar{x}_{lat} =
\begin{bmatrix}
v\\ 
p\\ 
r\\ 
\phi
\end{bmatrix}
=
\begin{bmatrix}
x_2\\ 
x_4\\ 
x_6\\ 
x_7
\end{bmatrix}
```

The Lateral section of the A matrix:

_Alat=A(5:8, 5:8)_

```math
Alat =
\begin{bmatrix}
   -0.1805 &   1.2713 & -84.9905 &   9.8089 \\
   -0.0286 &  -1.3460 &  0.5842  &        0 \\
    0.0077 &   0.0554 &  -0.5533 &        0 \\
         0 &   1.0000 &   0.0150 &        0 \\
\end{bmatrix}
\quad

Blat =
\begin{bmatrix}
         0  &       0 &   2.3012 &        0 &        0 \\
   -0.9486  &       0 &   0.3640 &   0.0407 &  -0.0407 \\
   -0.0199  &       0 &  -0.4081 &   0.7804 &  -0.7804 \\
         0  &       0 &        0 &        0 &        0 \\
\end{bmatrix}
```

Whose eigenvalues and eigenvectors:

```math
\lambda_{lat} = 
\begin{bmatrix}
-1.3873 + 0.0000i\\ 
-0.2918 + 0.7999i\\ 
-0.2918 - 0.7999i\\ 
-0.1088 + 0.0000i
\end{bmatrix}
\\
T_{lat} =
\begin{bmatrix}
   0.0588 + 0.0000i &  0.9995 + 0.0000i &  0.9995 + 0.0000i &  0.9897 + 0.0000i \\
   0.8089 + 0.0000i & -0.0181 + 0.0096i & -0.0181 - 0.0096i & -0.0156 + 0.0000i \\
  -0.0543 + 0.0000i &  0.0031 - 0.0074i &  0.0031 + 0.0074i &  0.0153 + 0.0000i \\
  -0.5825 + 0.0000i &  0.0178 + 0.0161i &  0.0178 - 0.0161i &  0.1417 + 0.0000i \\
\end{bmatrix}
```

Dutch role:
The damping ratio for the oscillatory pole is 0.3427, and this pole affects mainly p and q.

Here is a pole zero map of all of the modes.

![pzmap of modes](./images/aircraft_modes.png)

The 0 eigenvalues explains that is no dependency between ϕ and the dynamics.
