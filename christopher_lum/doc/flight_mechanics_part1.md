# Flight Mechanics 1

In this document a summary of vector calculus to establish the flat earth equations of motion for planes to circumnavigate the globe.

## The Rodrigues's rotation formula

Let u be the unrotated vector and n the axis around a theta angle will be rotated.
The resulting vector, v,

```math
\vec{v} = (1 - cos \theta) (\vec{u} \cdot \vec{n}) \vec{n} + cos \theta \cdot \vec{u} - sin \theta \cdot \vec{u} \times \vec{n}
```

### Matrix Notation

In matrix notation the cross product can be expressed as:


```math
\begin{bmatrix}
(\vec{n} \times \vec{u})_x\\ 
(\vec{n} \times \vec{u})_y\\ 
(\vec{n} \times \vec{u})_z
\end{bmatrix}

=

\begin{bmatrix}
n_y u_z - n_z u_y \\
n_z u_x - n_x u_z \\
n_x u_y - n_y u_x 
\end{bmatrix}

=

\begin{bmatrix}
0 & -n_z  & n_y \\ 
n_z & 0  & -n_x \\ 
-n_y & n_x  & 0
\end{bmatrix}

\begin{bmatrix}
u_x \\ 
u_y \\
u_z 
\end{bmatrix}
```

Such is:

```math
N \vec{u} = \vec{n} \times \vec{u}
```

And

```math
N (N \vec{u}) = N^2 \vec{u} = \vec{n} \times ( \vec{n} \times \vec{u} )
```

Decomposing u in a vector parallel to n and a perpendicular one and substituting in the Rodrigues' formula:

```math
\vec{v} = \vec{u} + sin \theta \cdot K\vec{u} + (1 - cos \theta) K^2 \vec{u} \quad | K | = 1
```

So the full rotation as a matrix can be expressed as:

```math
\mathbf{R} = \mathbf{I} + (sin \theta) \mathbf{N} + (1 - cos \theta) \mathbf{K}^2
```

#### Interesting facts

1. Eigenvalues of N are: ${ 0 }$ and ${ \pm i }$
1. Tr(I) = 3, TR(N) = 0, ${ Tr(\mathbf{n} \mathbf{n}^T) = 1 }$
1. ${ \mathbf{R} = cos \theta \cdot \mathbf{I} + (sin \theta) \mathbf{N} + (1 - cos \theta) \mathbf{n} \mathbf{n}^T }$
1. ${ \vec{n}(\vec{n} \cdot \vec{u}) = (\mathbf{n} \mathbf{n}^T ) \vec{u} }$
1. Using the double cross producto formula: ${ \vec{n} (\vec{n} \cdot \vec{u}) = \vec{u} + \vec{n} \times (\vec{n} \times \vec{u}) }$ 
1. Therefore, from above, ${ Tr(NN) = 1 - 3 (=Tr(1)) =  -2 }$

### The Extended Rodrigues' formula

Credit: Notes taken from this site: https://arxiv.org/pdf/1810.02999.pdf

Fact 1:


${ \theta }$ can be derived from the following facts:

```math
Tr(\mathbf{R}) = 3 - 2(1 - cos \theta)
```

Ergo

```math
cos \theta = \frac{Tr(\mathbf{R}) - 1}{2}
```

Fact 2:

```math
\mathbf{NR} = \mathbf{N} + sin \theta \cdot \mathbf{NN} + (1 - cos \theta) \cdot \mathbf{NNN}
```

```math
\mathbf{NN} = \mathbf{n} \mathbf{n}^T - 1
```
```math
\mathbf{N}\vec{n} = 0
```

```math
\mathbf{NNN} = -\mathbf{N}
```

```math
\mathbf{NR} = cos \theta \cdot \mathbf{N} + sin \theta \cdot \mathbf{NN}
```

```math
Tr(\mathbf{NR}) = - 2 \cdot sin \theta
```

```math
sin \theta = - \frac{Tr(\mathbf{NR})}{2}
```
The axis of rotation can be found solving:

```math
\mathbf{R} \vec{n} = 1 \cdot \vec{n}
```


## Euler Angles

- v: vehicle
- b: body

Yaw, Pitch and Roll (bank)

Yaw rotation around $$ Z_v $$ axis:

```math
C_{1/v}(\psi) = \begin{pmatrix}
 \cos \psi & \sin \psi & 0 \\ 
-\sin \psi & \cos \psi & 0 \\ 
 0 & 0  & 1 
\end{pmatrix}
```

Pitch Rotation around $$ y_1 $$:

```math
C_{2/1}(\theta) = \begin{pmatrix}
 \cos \theta & 0 & -\sin \theta \\ 
 0 & 1  & 0 \\
 \sin \theta & 0 & \cos \theta  \\ 
\end{pmatrix}
```

Banking around ${ x_2 }$:

```math
C_{b/2}(\phi) = \begin{pmatrix}
 1 & 0  & 0 \\
 0 &  \cos \phi & \sin \phi \\ 
 0 & -\sin \phi & \cos \phi  \\ 
\end{pmatrix} 
```

And all together Direction cosine matrix (DCM):

```math
DCM(\psi, \theta, \phi) = \begin{pmatrix}
\cos \theta \cos \psi                                 && \cos \theta \sin \psi && -\sin \theta \\

\cos \psi \sin \theta \sin \phi - \cos \phi \sin \psi && \cos \phi \cos \psi + \sin \theta \sin \phi \sin \psi && \cos \theta \sin \phi \\

\cos \phi \cos \psi \sin \theta + \sin \phi \sin \psi && -\cos \psi \sin \phi + \cos \phi \sin \theta \sin\psi && \cos \theta \cos \phi
\end{pmatrix}
```

One of the eigenvalues of DCM $$ C_{b/v} $$ is 1, so we can find an axis of rotation which is the corresponding eigenvector.

### In reverse

To get the angles from the Euler matrix:

- Yaw: ${ \psi = \arctan \frac{C_{12}}{C_{11}} }$
- Pitch: ${ \theta = \arcsin C_{13} }$
- Roll: ${ \phi = \arctan \frac{C_{23}}{C_{33}} }$

### Computing Euler Angles

From:

1. p: _roll rate_ comes from ailerons. -  ${ \ne \dot{\phi} }$
1. q: _pitch rate_ comes from elevator. -  ${ \ne \dot{\theta} }$
1. r: _yaw rate_ comes from rudder. -  ${ \ne \dot{\psi} }$


Recall each rotatation matrix F1/v, F2/1, Fb/2

The rotation of the body frame in respect to the NED frame:

```math
\vec{w}_{b/v}^b =
\begin{bmatrix}
p\\ 
q\\ 
r
\end{bmatrix}
```

```math
\vec{w}_{b/v}^b = \vec{w}_{b/2}^b + \vec{w}_{2/1}^b + \vec{w}_{1/v}^b
```

```math
\vec{w}_{1/v}^v = 
\begin{bmatrix}
0\\ 
0\\ 
\dot{\psi}
\end{bmatrix}
= 
\vec{w}_{1/v}^1
```

```math
\vec{w}_{2/1}^2 = 
\begin{bmatrix}
0\\ 
\dot{\theta}\\ 
0
\end{bmatrix}
= 
\vec{w}_{2/1}^1
```

```math

\vec{w}_{b/2}^b = 
\begin{bmatrix}
\dot{\phi}\\ 
0\\ 
0
\end{bmatrix}
= 
\vec{w}_{b/2}^2
```

```math
\vec{w}_{b/v}^b = \vec{w}_{b/2}^b + \vec{w}_{2/1}^2 + \vec{w}_{1/v}^1
```

```math
\vec{w}_{b/v}^b = \vec{w}_{b/2}^b + C_{b/2}(\phi) \vec{w}_{2/1}^2  + C_{b/2}(\phi)  C_{1/v}(\theta) \vec{w}_{1/v}^1
```

```math

\vec{w}_{b/v}^b =

\begin{bmatrix}
\dot{\phi}\\ 
0\\ 
0
\end{bmatrix}
+

\begin{bmatrix}
 1 & 0  & 0 \\
 0 &  \cos \phi & \sin \phi \\ 
 0 & -\sin \phi & \cos \phi  \\ 
\end{bmatrix} 


\begin{bmatrix}
0\\ 
\dot{\theta}\\ 
0
\end{bmatrix}

+


\begin{bmatrix}
 1 & 0  & 0 \\
 0 &  \cos \phi & \sin \phi \\ 
 0 & -\sin \phi & \cos \phi  \\ 
\end{bmatrix} 

\begin{bmatrix}
 \cos \theta & 0 & -\sin \theta \\ 
 0 & 1  & 0 \\
 \sin \theta & 0 & \cos \theta  \\ 
\end{bmatrix}

\begin{bmatrix}
0\\ 
0\\ 
\dot{\psi}
\end{bmatrix}
```

Operating:

```math
\vec{w}_{b/v}^b = 


\begin{bmatrix}
 \dot{\phi} -\dot{\psi} \sin \theta \\
 \dot{\theta} \cos \phi +  \dot{\psi}\cos \theta \sin \phi \\ 
 \dot{\psi} \cos \theta \cos \phi - \dot{\theta} \sin \phi  \\ 
\end{bmatrix}
```
Or 

```math
\vec{w}_{b/v}^b = 


\begin{bmatrix}
 1 && 0 && \sin \theta \\
 0 &&  \cos \phi  && \sin \phi \cos \theta  \\ 
 0 && -\sin \phi && \cos \phi \cos \theta   \\ 
\end{bmatrix} 

\begin{bmatrix}
\dot{\phi}\\
\dot{\theta}\\
\dot{\psi}
\end{bmatrix} 
```

Taking the inverse we get the Euler kinematical equation:

```math
\begin{bmatrix}
\dot{\phi}\\
\dot{\theta}\\
\dot{\psi}
\end{bmatrix} 

=

\begin{bmatrix}
 1 && \sin \phi \tan \theta && \cos \phi \tan \theta \\
 0 &&  \cos \phi  && -\sin \phi  \\ 
 0 && \sec \theta \sin \phi && \cos \phi \sec \theta   \\ 
\end{bmatrix} 


\begin{bmatrix}
p\\
q\\
r
\end{bmatrix} 

```
In a single line


```math
\dot{\Phi}=H(\theta,\phi) \vec{w}_{b/v}^b
```

But ${ \dot{\Phi} }$ is not a vector, since every component is referred to a different frame,
so H is **not a rotation matrix** . 



### Poisson's kinematical Equations


The variaton of the Direction Cosine Matrix:

```math
\dot{C_{b/v}} = -\Omega_{b/v}^b \cdot C_{b/v}
```

From the book of Stevens & Lewis, Control and Simulation of aircrats, the poisson'sk eq (the strapdown equation):


```math

\Omega_{b/v}^b = 
\begin{bmatrix}
 0 & -r &  q \\ 
 r &  0 & -p\\ 
-q &  p & 0 
\end{bmatrix}
```

There are 9 ODE's, but **it is an actual rotation matrix**.


## Tracking Attitude using Quaternions

### Introduction to Quaternions

Many definitions. In this lecture:

```math
q = 
\begin{bmatrix}
 \cos \frac{\theta}{2}  \\ 
 \vec{v}_{3x1} \cdot \sin \frac{\theta}{2}
\end{bmatrix}_{4x1} = 

\begin{bmatrix}
q_s \\
q_x \\
q_y \\
q_z \\
\end{bmatrix}
```

Some operations:

Norm:

```math
|q| = \sqrt{q_s^2 + q_x^2 + q_y^2 + q_z^2 }
```

Unit:

```math
q_{unit} = \frac{q}{|q|}
```

Conjugate:

```math
q^{*} = 
\begin{bmatrix}
q_s \\
-q_x \\
-q_y \\
-q_z \\
\end{bmatrix}
```
Inverse:

```math
q^{-1} = \frac{q^*}{|q|}
```

Multiplication:

if

```math
q_1 = 
\begin{bmatrix}
s_1 \\
\vec{v_1} \\
\end{bmatrix}
```

then,

```math
q_1 \otimes q_2 = 
\begin{bmatrix}
s_1 s_2 - <\vec{v_1},\vec{v_2}>\\
s_1 \vec{v_2} + s_2 \vec{v_1} + \vec{v_1} \times \vec{v_2} \\
\end{bmatrix}
```

### Attitude Representation with Quaternions

Consider the DCM ${ C_{b/v} = c_{ij} }$

#### DCM -> Quaternion

1. Calculate the intermediate quantity

```math

\tilde{q} = 
\begin{bmatrix}
\tilde{q_s}\\ 
\tilde{q_x}\\ 
\tilde{q_y}\\ 
\tilde{q_z}
\end{bmatrix}

=

\begin{bmatrix}
\sqrt{\frac{1}{4}(1 + C_{11} + C_{22} + C_{33} )} \\ 
\sqrt{\frac{1}{4}(1 + C_{11} - C_{22} - C_{33} )}\\ 
\sqrt{\frac{1}{4}(1 - C_{11} + C_{22} - C_{33} )}\\ 
\sqrt{\frac{1}{4}(1 - C_{11} - C_{22} + C_{33} )}
\end{bmatrix}

```

1. Find the maximum value of the above

1. Follow the table below:


| Max               | $$ q_s $$         | $$ q_x $$         | $$ q_y $$          | $$ q_z $$         |
|-------------------|-------------------|-------------------|--------------------|-------------------|
| $$ \tilde{q_s} $$ | $$ \tilde{q_s} $$                         | $$ \frac{C_{23}-C_{32}}{4 \tilde{q_s}} $$ | $$ \frac{C_{31}-C_{13}}{4 \tilde{q_s}} $$ | $$ \frac{C_{12}-C_{21}}{4 \tilde{q_s}} $$ | 
| $$ \tilde{q_x} $$ | $$ \frac{C_{23}-C_{32}}{4 \tilde{q_x}} $$ | $$ \tilde{q_x} $$                         | $$ \frac{C_{12}-C_{21}}{4 \tilde{q_x}} $$ | $$ \frac{C_{31}-C_{13}}{4 \tilde{q_x}} $$ |
| $$ \tilde{q_y} $$ | $$ \frac{C_{31}-C_{13}}{4 \tilde{q_y}} $$ | $$ \frac{C_{12}+C_{21}}{4 \tilde{q_y}} $$ | ${ \tilde{q_y} }$                         | $$ \frac{C_{23}+C_{32}}{4 \tilde{q_y}} $$ |
| $$ \tilde{q_z} $$ | $$ \frac{C_{12}-C_{21}}{4 \tilde{q_z}} $$ | $$ \frac{C_{31}+C_{13}}{4 \tilde{q_z}} $$ | $$ \frac{C_{23}+C_{32}}{4 \tilde{q_z}} $$ | $$ \tilde{q_z} $$                         |

Note: We found some discrepancy with this page,

https://www.vectornav.com/resources/inertial-navigation-primer/math-fundamentals/math-attitudetran

but it might be due to a different definition of the quaternions.



#### Quaternion -> DCM

```math
C_{b/v} = 
\begin{bmatrix}
 q_s^2 + q_x^2 - q_y^2 - q_z^2 & 2(q_x q_y + q_z q_s) & 2(q_x q_z - q_y q_s ) \\ 
 2(q_x q_y - q_z q_s) & q_s^2 - q_x^2 + q_y^2 - q_z^2  & 2(q_y q_z + q_x q_s )\\
 2(q_x q_z + q_y q_s) & 2(q_y q_z - q_x q_s) & q_s^2 - q_x^2 - q_y^2 + q_z^2
\end{bmatrix}
```


#### Euler Angles -> Quaternion

Euler Angle Phi( phi, theta, psi) -> DCM (Cbv) -> dcm2quat -> q

#### Quaternion -> Euler angles

Q -> quat2dcm -> extract angles -> Phi

Angle extraction:

```math
\phi = \arctan \frac{C_{23}}{C_{33}} \\
\theta = - \arcsin C_{13} \\
\psi = \arctan \frac{C_{12}}{C_{11}}
```

#### Rate of variation

Given IMU data:

```math
\vec{w}_{b/v}^b =
\begin{bmatrix}
p\\ 
q\\ 
r
\end{bmatrix}

=

\begin{bmatrix}
w_x\\ 
w_y\\ 
w_z
\end{bmatrix}
```

The rate of change,

$$ \dot{q} = \frac{1}{2} \Omega \cdot q $$

where,

```math
\Omega = 
\begin{bmatrix}
0 & -w_x  & -w_y & -w_z \\ 
w_x & 0 & w_z & -w_y \\ 
w_y & -w_z & 0 & w_x \\
w_z & w_y & -w_x & 0 
\end{bmatrix}
```

Alternatively

```math
\dot{q} = \frac{1}{2} 
\begin{bmatrix}
q_1 & q_4  & -q_3 &  q_2 \\ 
q_2 & q_3  &  q_4 & -q_1 \\ 
q_3 & -q_2 &  q_1 &  q_4 \\
q_4 & -q_1 & -q_2 & -q_3
\end{bmatrix}

\begin{bmatrix}
0  \\ 
w_x\\ 
w_y\\ 
w_z
\end{bmatrix}
```

#### MATLAB 

In matlab we find these functions amidst others:

```matlab
Q   = dcm2quat(dcm);
DCM = quat2dcm(Q);
Q   = angle2quat(psi, theta, phi);  	% Notice angle order
[psi, theta, phi] = quat2angle(q);
[psi, theta, phi] = dcm2angle(dcm);
```

## Homegenous Coordinates

![Frame of Reference](./images/homogeneous.png)

We know that

$$
\vec{r}_{v/r}^r = \vec{r}_{v/b}^b + \vec{r}_{b/r}^r
$$

To express everything referred to the same frame:

$$
\vec{r}_{v/r}^r = C_{r/b} \cdot \vec{r}_{v/b}^b + \vec{r}_{b/r}^r
$$

Which can be expressed in homogenous coordinates as:

$$


\left[
  \begin{array}{c}
    \vec{r}_{v/r}^r  \\
    \hline
    1
  \end{array}
\right ]

=

\left[
  \begin{array}{c|c}
    C_{r/b} & \vec{r}_{b/r}^r \\ 
    \hline
    0 & 1
  \end{array}
\right ]

\left[
  \begin{array}{c}
    \vec{r}_{v/b}^b \\
    \hline
    1
  \end{array}
\right ]
 
$$

## The Flat Earth Equations of Motion

Due technical issues with markdown those equations are written down in the [aside document](./eom.pdf).


