%% Initial section
clear
clc
close all

%% Load gyro data
% Which I don't possess
% load('GyroData.mat') % By Christopher Lum

load("rpy_9axis.mat")
t = linspace(1,100,1600)';

p = sensorData.AngularVelocity(:,1);
q = sensorData.AngularVelocity(:,2);
r = sensorData.AngularVelocity(:,3);



%% Define initial conditions for Euler angles
phi0    = 0;
theta0  = 0.0059;
psi0    = 0;

%% Initial condition for Poisson's Kinematical Equations


C1v = [
    cos(psi0)   sin(psi0)   0;
    -sin(psi0)  cos(psi0)   0;
    0           0           1;
    ];

C21 = [
    cos(theta0) 0   -sin(theta0);
    0           1   0           ;
    sin(theta0) 0   cos(theta0) ;
    ];

Cb2 = [
    1   0           0           ;
    0   cos(phi0)   sin(phi0)   ;
    0   -sin(phi0)  cos(phi0)   ;
    ];

Cbv0 = Cb2*C21*C1v;

%% Initial condition for Quaternion kinematic equations

q0 = angle2quat(psi0, theta0, phi0);