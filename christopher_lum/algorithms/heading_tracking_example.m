%% Angle Manipulation Example

clear
clc
close all

%% Initial Data

a2r = pi / 180;

phi   =  25 * a2r;
theta = 130 * a2r;
psi2  =  70 * a2r;

%% Direct Conversions

dcm = angle2dcm(psi2, theta, phi);
q   = angle2quat(psi2, theta, phi);

%% Comparisons
% Check output by removing commas

% Among unique representations
dcm2quat(dcm);
quat2dcm(q);

% with original Euler angles
[y,p,r] = quat2angle(q);

y/a2r;
p/a2r;
r/a2r;

% See the different result that drives the solid to the same final
% position.



%% Lets compute using DCM

[V,D] = eig(dcm);

% Since the eigenvector associated to eigenvalue 1 is the third one
% we know the axis of rotation shall be

% Rotation axis
V(:,3);     % -0.4845    0.8706    0.0851

% Euler angles from dcm

r = atan2(dcm(2,3), dcm(3,3)) /a2r;     % -155
p = -asin(dcm(1,3)) / a2r;              % 50
y = atan2(dcm(1,2), dcm(1,1)) / a2r;    % -110

%% Lets compute using Quaternions

% Translational rotation
rot = 2 * acos(q(1));
rot / a2r;   % 126.4490

srot2 = sin(rot/2);

% Exctract quaternion vector (qx, qy, qz)
q(2:4) / srot2  % -0.4845    0.8706    0.0851


%% Extended Rodrigues's Rotation Formula

N = [
    0       -V(3,3)  V(2,3);
    V(3,3)   0       -V(1,3);
    -V(2,3) V(1,3)  0;
    ];

R = eye(3) + sin(rot)*N + (1-cos(rot))*N^2;

cr = (trace(R) - 1 )/2;    
sr = -1 * trace (N*R)/2;

atan2(sr,cr) / a2r;       % 126.4490 Rotation angle
