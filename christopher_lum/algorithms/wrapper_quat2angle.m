function [Phi] = wrapper_quat2angle(q)

[psi, theta, phi] = quat2angle(q');

% Rearrange
Phi = [phi; theta; psi];
