%% Find the axis of rotation

%% Define Euler rotations
psi = 70*pi/180;
theta = 130*pi/180;
phi = 25*pi/180;

%% Compute Cbv

cpsi = cos(psi);
spsi = sin(psi);

ctheta = cos(theta);
stheta = sin(theta);

cphi = cos(phi);
sphi = sin(phi);

C1v = [
    cpsi  spsi 0;
    -spsi cpsi 0;
    0     0    1
    ];

C21 = [ 
    ctheta 0 -stheta;
    0 1 0;
    stheta 0 ctheta
    ];

Cb2 = [
    1 0 0;
    0 cphi  sphi;
    0 -sphi cphi
    ];

Cbv = Cb2*C21*C1v;

%% Get eigenvalues and eigenvector

[V,D] = eig(Cbv);

idxUnityEigenvalue = 0;
% There must be a 1 eigenvalue
for vidx=1:3
    if D(vidx,vidx) == 1
        idxUnityEigenvalue = vidx;
    end
end

assert(idxUnityEigenvalue > 0)

% This is the axis of rotation we were looking for.
v = V(:, idxUnityEigenvalue)
