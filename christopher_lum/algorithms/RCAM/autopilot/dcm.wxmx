PK     D��V�B�H         mimetypetext/x-wxmathmlPK     D��ViQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     D��V���[�  �     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 23.04.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="4">

<cell type="code">
<input>
<editor type="input">
<line>C1v:matrix(</line>
<line> [cos(ψ),sin(ψ),0], </line>
<line> [-sin(ψ),cos(ψ),0], </line>
<line> [0,0,1]</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(C1v)	" userdefinedlabel="C1v">(%o8) </lbl><tb roundedParens="true"><mtr><mtd><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><fnm>−</fnm><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>C21:matrix(</line>
<line>    [cos(θ), 0, -sin(θ)],</line>
<line>    [0,1,0],</line>
<line>    [sin(θ), 0, cos(θ)]</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(C21)	" userdefinedlabel="C21">(%o3) </lbl><tb roundedParens="true"><mtr><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><n>0</n></mtd><mtd><fnm>−</fnm><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><n>0</n></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>Cb2:matrix(</line>
<line>    [1,0,0],</line>
<line>    [0, cos(Φ), sin(Φ)],</line>
<line>    [0, -sin(Φ), cos(Φ)]</line>
<line>);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(Cb2)	" userdefinedlabel="Cb2">(%o4) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn></mtd><mtd><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><fnm>−</fnm><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>Cbv:Cb2.C21.C1v;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(Cbv)	" userdefinedlabel="Cbv">(%o10) </lbl><tb roundedParens="true"><mtr><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fnm>−</fnm><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></mtd></mtr><mtr><mtd><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn><fnm>−</fnm><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn><fnm>+</fnm><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd></mtr><mtr><mtd><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn><fnm>+</fnm><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>ψ</v></p></r></fn><fnm>−</fnm><fn><r><fnm>sin</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>ψ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>Φ</v></p></r></fn><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd></mtr></tb>
</mth></output>
</cell>

</wxMaximaDocument>PK       D��V�B�H                       mimetypePK       D��ViQ#4  4  
             5   format.txtPK       D��V���[�  �               �  content.xmlPK      �   o    