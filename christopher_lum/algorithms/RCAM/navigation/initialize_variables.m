x0 = [
    85;     % Approx 165 knots
    0;
    0;
    0;
    0;
    0;
    0;
    0.1;    % Approx 5.73 deg
    0;
    ];

u = [
    0;
    -0.1;   % Approx -5.73 deg
    0;
    0.08;   % Recall minimum for throttles are 0.5*pi/180 = 0.0087
    0.08;
    ];

TF = 3*60;  % Final simulation time

% Define min max mavlues

% --------------- 1. CONTROL LIMITS/SATURATION --------------- 
% Note that these can alternately be enforced in Simulink
u1min = -25*pi/180;
u1max = 25*pi/180;

u2min = -25*pi/180;
u2max = 10*pi/180;

u3min = -30*pi/180;
u3max = 30*pi/180;

% u4min = 0.5*pi/180;
u4min = 0.5*pi/180; %
u4max = 10*pi/180;

u5min = 0.5*pi/180;
u5max = 10*pi/180;

%% Parameters

% Saint Crepin:
% 44.70170 and longitude 6.60028
% N 44° 42' 6.1'' Longitude: E 6° 36' 1''
lon0 = deg2rad(dms2degrees([6 36 1]));
lat0 = deg2rad(dms2degrees([44 42 6.1]));
h0 = 1000;

Xgeodetic0 = [lat0;lon0;h0];    % Initial position and altitude
Xned0 = [0;0;-h0];

%% noexecution
% temp2 = load('trim_values_straight_level.mat');
% Xo = temp2.Xo;
% 
Xo = [Xned0;
    Xgeodetic0];



disp('FINISHED')