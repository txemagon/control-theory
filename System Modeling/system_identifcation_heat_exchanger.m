%% Estimating Transfer Function Models for a Heat Exchanger
% com.mathworks.mlwidgets.html.HtmlComponentFactory.setDefaultType('HTMLRENDERER');

load iddemo_heatexchanger_data    % Load demo 

% Collect data
data = iddata (pt, ct, Ts);
data.InputName   = '\Delta CTemp';
%% 
data.InputUnit   = 'C';
data.OutputName  = '\Delta Ptemp';
data.OutputUnit  = 'C';
data.TimeUnit    = 'minutes';
plot (data)

% Transfer Function Estimation
sysTF = tfest(data, 1, 0, nan)

% Comparison and residue correlation

set(gcf, 'DefaultAxesTitleFontSizeMultiplier', 1,...
    'DefaultAxesTitleFontWeight', 'normal',...
    'Position', [100 100 780 520]);
resid(sysTF, data);

clf
compare(data, sysTF)

%% Transfer Function Estimation from an Initial System