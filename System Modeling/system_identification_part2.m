%% From: Linear System Identification | System Identification, Part 2



% Create  "real" system dynamics
% ==============================

s = tf('s');
Greal = 5*(s + 1)/(s^2 + 4*s + 5) * exp(-0.1*s)



% Collect data
% ============

dt = 0.01;
t = 0:dt:5;

% Build the step input
u = ones(length(t), 1);
u(1:1/dt) = 0;

% Simulate a step response
yreal = lsim(Greal, u, t);

plot (t, [u, yreal], 'LineWidth', 4);
axis ([0 5 0 1.4]);
grid on
legend(['u'; 'y']);




% Choose Model Structure
% ======================

% At least 2nd order
% Transport delay


% Identify model by fitting it to the data
% ========================================

% Fit data to a transfer function with unknown delay term
data = iddata(yreal, u, dt);
Gest = tfest(data, 2, 0, NaN)   % 2 poles, 0 zeros, NaN (undefined) time delay



% Validate the model
% ==================

opt = compareOptions;   % Create an option set for comparison.
opt.InitialCondition = 'z';
compare(data, Gest, opt);
set (findall(gca, 'Type', 'Line'), 'LineWidth', 4);
grid on



% Identify a state space model
% ============================

% Estimate the delay term
delay_samples = delayest(data);

% Remove delay from output
yreal_no_delay = yreal(delay_samples+1:end);

% Repackage offset data
data_offset = iddata(yreal_no_delay, u(1:end-delay_samples), dt);

% Fit data to a state space model of unknown order
Gss = ssest(data_offset, 1:10) % Try between 1 and 10 models
% Convert to tf and add in the delay term
Gest = tf(Gss) * exp(-delay_samples * dt * s)


% Validate the model
% ==================

opt = compareOptions;
opt.InitialCondition = 'z';
compare (data, Gest, opt);
set (findall(gca, 'Type', 'Line'), 'Linewidth', 4);
grid on