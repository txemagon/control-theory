%% Cruise Discrete Control 

clear all

% Rise Time < 5s
% Overshoot < 10%
% Steady state error < 2%

%% Physical conditions

m = 1000;
b = 50;

%% Model
s = tf('s')
P_cruise = 1/(m*s+b);

Ts = 1/50;
dP_cruise = c2d(P_cruise, Ts,'zoh')

%% Root Locus in the z-plane

% Providing natural frequency and damping ratio
% $$ \omega_n \ge \frac{1.8}{T_r} $$
% $$ \zeta \ge \sqrt{\frac{\ln^2(M_p)}{\pi^2 + \ln^2(M_p)}} $$
% wn=0.36 rads/s b=0.6. In rads/sample:

Wn = 0.0072
zeta = 0.6;

rlocus(dP_cruise)
zgrid(zeta,Wn)
% axis([-1 1 -1 1])
axis([0.95 1 -.1 .1])

% Choose Poles

[K, poles] = rlocfind(dP_cruise)

% Selecting a point close to .99

%% Control design

K = 451.1104;
sys_cl = feedback(K*dP_cruise,1);
r = 10;

figure
step(r*sys_cl,10);

% Steady-state error is 11%

%% Compensation using lag controller

% $$ C_{lag}(z) = K_d\frac{z-z_0}{z-z_p} $$

% We choose the zero at 0.999
% Steady state error reduces at zp=(1-z0)/(1-zp)
% zp = 0.9998 to reduce error 5 times
% Kd = (1-zp)/(1-z0)=0.2

z = tf('z',Ts)
C = 0.2*(z-0.999)/(z-0.9998)

rlocus(C*dP_cruise)
zgrid(zeta,Wn)
axis([0.98 1 -0.01 0.01])

[K, poles] = rlocfind(C*dP_cruise)

% We choose a point near 0.99

%% Final result

K = 2.4454e+03;
sys_cl = feedback(K*C*dP_cruise,1);
r = 10;
step(r*sys_cl,10);