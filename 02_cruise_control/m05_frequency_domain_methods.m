%% Cruise control with frequency domain methods

clear all

%% Physical data

m = 1000;
b = 50;
u = 500;

Kp = 1;
s = tf('s');
P_cruise = 1/(m*s+b);

% Check if open loop response is stable (required)
C = Kp;
step(u*C*P_cruise)

% As we can see is stable, and we can generate the Bode plot

%% Study of the system
bode(C*P_cruise);

% steady state error: $$ sse = \frac{1}{1 + M_{w_n \to 0}} $$ 
% In the plot G(0) = -34dB

M = 10^(-34/20); %> M = 0.02

% We can also see the steady state error...
r = 10;
sys_cl = feedback(C*P_cruise,1);
step(r*sys_cl);

% To achieve sse <0.02 Mw=0 > 49 , G = 33.8dB
M = (1 - 0.02)/0.02;

% Gain has to be increased in 67.8dB => 2455

Kp = 2500;
C = Kp;
bode(C*P_cruise);

sys_cl = feedback(C*P_cruise,1);
step(r*sys_cl);

% Since we obtain a very small rise time we will use a

%% Lag Compensator

% The steady state error will be reduced in z0/p0

Kp = 1000;
zo = 0.1;
po = 0.02;

C_lag = (s+zo)/(s+po);
bode(Kp*C_lag*P_cruise);


%% Step response
sys_cl = feedback(Kp*C_lag*P_cruise,1);
t = 0:0.1:20;
step(r*sys_cl,t);