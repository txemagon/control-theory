%% Cruise Control: System Modeling
% From: https://ctms.engin.umich.edu/CTMS/index.php?example=CruiseControl&section=SystemModeling

% $$ v = \dot{x} $$
% $$ a = \dot{v} = \ddot{x} $$

%% Syste Equations
% $$ m\dot{v} + bv = u $$
% y = v

%% System Parameters

% (m) Vehicle mass 1000 kg
% (b) Damping coefficient 50 Ns/m

%% State-space model

% $$ \dot{\bold{x}} = \left[ \dot{v} \right ] = \left[ \frac{-b}{m} \right]
% \cdot \left[v \right ] + \left[ \frac{1}{m} \right ] \cdot \left[ u
% \right ] $$
%
% $$ y = \left[1 \right ] \cdot \left[v \right ] $$

m = 1000;
b = 50;

A = -b/m;
B = 1/m;
C = 1;
D = 0;

cruise_ss = ss(A,B,C,D);

%% Transfer function model

% $$ P(s) = \frac{V(s)}{U(s)} = \frac{1}{ms + b}  \left[ \frac{m/s}{N} \right ] $$

s = tf('s');
P_cruise = 1 /(m*s + b);


%% System analysis

% Perfomance Specifications
%
% rise time < 5s
% Overshoot < 10%
% steady-state error < 2%

%% Open-loop step response

m = 1000;
b = 50;
u = 500;

s = tf('s');
P_cruise = 1/(m*s+b);

step(u*P_cruise) 
pzmap(P_cruise)
axis([-1 1 -1 1])
bode(P_cruise)



