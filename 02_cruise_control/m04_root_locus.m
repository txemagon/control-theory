%% Cruise Control with Root Locus

% Commands: tf, rlocus, feedback, step, sgrid
%
% Open Loop TF: $$ P = \frac{1}{ms + b} $$
% Closed Loop TF:
% $$ G = \frac{KP}{1 + KP} $$
% $$ G = \frac{K_p}{ms + (b + K_p)}  $$

% Criteria:
% Tr less than 5 s
% Overshoot less than 10%
% Steady-state error less than 2%

% $$ \omega_n \ge \frac{1.8}{T_r} $$
% $$ \zeta \ge \sqrt{\frac{\ln^2(M_p)}{\pi^2 + \ln^2(M_p)}} $$


%% Physical data
clear all

m = 1000;
b = 50;
r = 10;

s = tf('s')
P_cruise = 1/(m*s + b);

%% Requirements

% Given
Tr=5    % seconds
Mp=0.1  % 10%

% Calculated
wn = 1.8/Tr
zeta = sqrt(log(Mp)^2/(pi^2 + log(Mp)^2))

%% First analysis

rlocus(P_cruise)
axis([-0.6 0 -0.6 0.6]);
sgrid(zeta, wn)

%% Graphical choosing a point

[Kp, poles]=rlocfind(P_cruise)

% After clicking in z=-4 I got Kp = 350, poles = -0,4

%% The response of the designed compensator

Kp = 350.2419;
sys_cl = feedback(Kp*P_cruise,1);
t = 0:0.1:20;
step(r*sys_cl, t)

%% Lag Controller

% $$ C(s) = \frac{s+z_0}{s + p_0} $$

% Without gain, the closeed loop
% $$ \frac{Y(s)}{U(s)} = \frac{s + z_0}{ms^2 + (b + mp_0)s + bp_0} $$

% Including the gain:
% $$ \frac{Y(s)}{U(s)} = \frac{K_p (s + z_0)}{ ms^2 + (b + mp_0 + K_p)s + (bp_0 + K_pz_0 )} $$

% The steady state error will be reduced by a factor of z0/p0 and must be
% close together

zo = 0.3;
po = 0.03;

s = tf('s');
C_lag = (s+zo)/(s+po);

rlocus(C_lag * P_cruise);
axis([-0.6 0 -0.4 0.4])
sgrid(0.6, 0.36);

%% Choosing a gain

[Kp, poles]=rlocfind(C_lag*P_cruise)

% Choosing somewhere about -.4
% we get  k=1293.6

%% Step response of close loop

Kp = 1293.6;
sys_cl = feedback(Kp*C_lag*P_cruise,1);
t = 0:0.1:20;
step(r*sys_cl, t)
axis([0 20 0 12])