%% State-Space Control

clear all

% Rise Time < 5s
% Overshoot < 10%
% Steady state error < 2%


%% Physical conditions

m = 1000;
b = 50;


%% System Model

A = [-b/m];
B = [1/m];
C = [1];
D = [0];

sys = ss(A,B,C,D);

p1 = -1.5;              % Arbitrary pole placement
K = place(A,B,[p1])    

sys_cl = ss(A-B*K,B,C,D);

%% Simulation
t = 0:0.1:10;               % Time frame
u = 500*ones(size(t));      % Step input
x0 = [0];                   % Initial condition

lsim(sys_cl, u, t, x0);
axis([0 10 0 0.35])


%% Compensate for the scale factor
Nbar = rscale(sys,K)*10/500;
sys_cl = ss(A-B*K,B*Nbar,C,D);

lsim(sys_cl,u,t,x0);
axis([0 10 0 11])
