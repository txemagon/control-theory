%% Modeling a magnetically suspended ball 

%% Modeling

A = [ 0  1  0
     980 0 -2.8
      0  0 -100];
  
B = [ 0
      0
      100 ];
  
C = [ 1 0 0];


%% Stability

% Poles are eigenvalues of A
poles = eig(A)   % One pole on the right hand plane


% Simulate linear response with a non-zero condition
t = 0:0.01:2;
u = zeros(size(t));
x0 = [0.01 0 0];

sys = ss(A,B,C,0);

[y,t,x] = lsim(sys,u,t,x0);
plot(t,y)
title('Open-Loop Response to Non-Zero Initial Condition')
xlabel('Time (sec)')
ylabel('Ball Position (m)')

%% Controlability and Observability

% If the contralability matrix has full rank the system is controllable.
rank(ctrb(A,B))
rank(ctrb(sys))

% A system (A,B) is controllable if (A', B') is obserbable
% Eigenvalues of (A - BK) are  poles of the closed-loops

% If we want...
% settling time < 0.5s
% overshoot < 5%

% zeta = 0.7 / 45º
% sigma = 10 > 4.6 * 2

% Let's try
p1 = -10 + 10i;
p2 = -10 -10i;
p3 = -50;

K = place(A,B,[p1,p2,p3]);
sys_cl = ss(A-B*K,B,C,0);

lsim (sys_cl, u, t, x0)
xlabel('Time (sec)')
ylabel('Ball Position (m)')

% To reduce overshoot...
p1 = -20 + 20i;
p2 = -20 - 20i;
p3 = -100;

K = place(A,B,[p1,p2,p3]);
sys_cl = ss(A-B*K,B,C,0);

lsim (sys_cl, u, t, x0)
xlabel('Time (sec)')
ylabel('Ball Position (m)')

% acker replaces place when you wnat to set two poles at the same position

%% Introducing the reference input

t = 0:0.01:2;
u = 0.001*ones(size(t));

sys_cl = ss(A-B*K,B,C,0);

lsim(sys_cl,u,t);
xlabel('Time (sec)')
ylabel('Ball Position (m)')
axis([0 2 -4E-6 0])

% Substracting Kx from the reference gives a negative output
% The solution is to let r be multiplied a factor N

Nbar = rscale(sys,K)

lsim (sys_cl, Nbar *u, t)
title('Linear Simulation Results (with Nbar)')
xlabel('Time (sec)')
ylabel('Ball Position (m)')
axis([0 2 0 1.2*10^-3])


%% Summary

% A drawing of the state space is accomplished
% We estimate the open-loop poles via the eigenvalues
% We determinee the controlability of the system with ctrb and rank
% Choose the desired place location of your poles and find gain via place
% Find out step response deviation with lsim
% Obtain Nbar in order to scale the reference with rscale


%% Observer Design

% e' = (A-LC)e
% Choose observer gain L.Poles five times farther to the left in order to
% be quicker.

op1 = -100;
op2 = -101;
op3 = -102;

% Duality observability and controllability => Transpose A and replace B by
% C

L = place (A',C', [op1 op2 op3])'
