%% Lagging Effect Associated with a Hold
% From: https://ctms.engin.umich.edu/CTMS/index.php?aux=Extras_Lageff

s = tf('s');
sys = 1 / (s^2 + 10*s + 20);

Ts =  0.2;
sys_d = c2d(sys, Ts);

step(sys,2)         % plots continous output response
hold on
[x,t] = step(sys_d,2);
plot(t,x,'ro')
hold off

% When using an impulse the discrete function doesn't match anymore the
% input

impulse(sys,2)
hold on
[x,t] = impulse(sys_d,2);
plot(t,x,'ro')
hold off