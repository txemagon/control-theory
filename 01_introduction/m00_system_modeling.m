%% System Modeling
% From: Introducition: System Modeling
% At: https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=SystemModeling

%% Spring and Damped Mass

% State Space Representation

m = 1;
k = 1;
b = 0.2;
F = 1;

A = [0 1; -k/m -b/m];
B = [0 1/m]';
C = [1 0];
D = [0];

sys = ss(A,B,C,D)

% Transfer Function

s = tf('s');
sys = 1 / (m*s^2 + b*s + k)

%% Electrical Systems
% On an RLC circuit, we now:
% $ V(t) - R \cdot i - L \frac{di}{dt} - \frac{1}{C} \int i \cdot dt = 0 $
%
% Or:
%
% $
% \dot{\vec{x}} =
% 
% \begin{bmatrix}
% 
% i \\ 
% \frac{di}{dt}
% 
% \end{bmatrix}
% 
% =
% 
% \begin{bmatrix}
% 
% 0 & 1 \\ 
% -\frac{1}{LC} & -\frac{R}{L}
% 
% \end{bmatrix}
% 
% \begin{bmatrix}
% 
% q \\ 
% i
% 
% \end{bmatrix}
% 
% +
% 
% \begin{bmatrix}
% 
% 0 \\ 
% \frac{1}{L}
% 
% \end{bmatrix}
% 
% V(t)
% $
%
% And the Transfer Function
% 
% $ G(s) = \frac{I(s)}{V(s)} $
% $ \frac{I(s)}{V(s)} = \frac{s}{Ls^2+Rs + \frac{1}{C}} $
%
% In state-space representation:
%
% $ G(s) = C (sI-A)^{-1}B + D $

