%% Frequency Domain Methods for COntroller Design
%
% Take open-loop frequency response to predict closed-loop response
% behaviour.
%
% key commands: bode, nyquist, margin, lsim, step, feedback,
% controlSystemDesigner

%% Gain and Phase Margin

% Frequency response analyses how a sinusoidal input is scaled and shifted
% by the system.

% Robustness: How far we are from becoming unstable (instability)
%
% Phase margin: Amount of change in open-lopp phase shift required to make
%               the closed-loop system unstable.
%               It also measures tolerance to time delay.
%               Maximum delay: $ \tau_d = \frac{PM}{\omega_{gc}} $
%               Wgc: Frequency for 0db amplitude. Gain crossover frequency

%
% Time delay:   Can be represented as a block of mangitude 1 and 
%               phase: $ -\omega \tau_d $

% Phase Margin: Amount of phase lag at 0dB to reach 180 degrees
% Gain Margin:  Amount of gain to reach 0dB at -180 degrees (phase
% crossover frequency, $\omega_{pc}$)

% Adding gain only rises the diagram

s = tf('s');
G = 50 / (s^3 + 9*s^2 + 30*s + 40);

bode (G)
grid on
title ('Bode Plot with no Gain')

bode (100*G)
grid on
title ('Bode Plot with Gain = 100')

%% Bandwidth frequency

% It's the frequency at which the closed-loop magnitude drops 3 dB below
% its magnitude at DC

% When working with frequency response we analize the open-loop response.
% Thus, we will use -6 to -7.5 dB assuming simething in between -135 deg
% and -225 deg
% Inputs below bandwith frequency ($ \omega_{bw} $) are well tracked  by
% the system.

% Let's see the importance of bandwidth
G = 1 / (s^2 + 0.5*s + 1);
bode (G)

% Notice -3dB are lost at 1.4 rad/s
% Let's simulatwe the response with lsim

G= 1 / (s^2 + 0.5*s + 1);
w = 0.3;
t = 0:0.1:100;
u = sin(w*t);
[y,t] = lsim(G,u,t);
plot(t,y,t,u)
axis([50 100 -2 2])

% Output blue, input red
G = 1/(s^2 + 0.5*s + 1);
w = 3;
t = 0:0.1:100;
u = sin(w*t);
[y,t] = lsim(G,u,t);
plot(t,y,t,u)
axis([90 100 -1 1])


%% Nyquist diagram

% Predicts stability and performance of the closed-loop observing the
% open-loop behaviour. It's an aid when Bode is confusing.

% Cauchy criterion: A closed contour mapped through a complex function, the
% number of times the plot encircles the origin is equal to the number of
% zeros munus the number of poles enclosed by the frequency contour

% In closed loop we are interested in G(s) / (1 + G(s) )
% If 1 + G(s) encircles the origin, then G(s) encircles -1.

s = tf('s')
G = 0.5 / (s - 0.5);
nyquist (G)
axis([-1 0 -1 1])

G = (s + 2)/(s^2);
nyquist(G)
nyquist1(G)

%% Closed-Loop Performance from Bode Plots

% Assumptions:
%
% - The system must be stable in open-loop if using Bode plots.
% - For canonical second-order systems, the closed-loop damping ratio is
% approximately equals the PM divided by 100 if 0<PM<60
% Bandwidth is similar lo natural frequency

% For a controller C and a plant P, some specifications:
%
% - Zero steady-state error
% - Maximum overshoot < 40%
% - Settling time < 2s

P = 10 / (1.25*s + 1);
bode(P)

% Rise time = 1.8/w_n = 0.18
% Damping ration = PM/100 if PM < 60 and 2nd order systems. This is first
% order

% Steady state error constant: where low frequency intersects w=1 => 0sse
% The gain is 20dB (magnitude 10). The constant for the error function is
% 10. Steady-state error is 1/(1+Kp) = 1/ 11 = 0.091

sys_cl = feedback(P,1);
step(sys_cl)
title('Closed-Loop Step Response, No Controller')

% With an integrator
 P = 10 / (1.25*s + 1);
 C = 1/s;
 bode(C*P, logspace(0,2))
 
 % Since the phase margin is small we'll add a zero
 
 P = 10 / (1.25*s + 1);
 C = (s + 1)/s;
 bode(C*P, logspace(0,2))
 
 
 %% Nyquist Criterion
 
 G = (s^2 + 10*s + 24)/(s^2 - 8*s + 15);
 
 roots([1 -8 15])
 
 % Poles of the open loop transfer function are two positive
 % We need two counterclockwise (N=-2) encirclements of the Nyquist diagram
 % to have a stable closed-loop system (Z = P + N).
 
 nyquist(G)
 
 % 2 encirclement so stable
 
 nyquist(20*G)
 
 % 2 encirclement so stable
 
 nyquist(0.5*G)
 
 % Now unstable. Indeed becomes unstable at .8 gain.
 
% GAIN MARGIN
 
 % We already defined the gain margin as the change in open-loop gain
 % expressed in decibels (dB), required at -180 degrees of phase shift to
 % make the open-loop magnitude equal 0 dB. 
 
 % The distance from G(180º) and -1 is the gain margin
 
 G = 50/(s^3 + 9*s^2 + 30*s + 40);
 nyquist(G)
 
 w = sqrt(30);
polyval(50,j*w)/polyval([1 9 30 40],j*w)
% https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlFrequency

a=4.6
nyquist(a*g)

%% PHASE MARGIN