%% Closed-Loop Poles
%% From: https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlRootLocus

% If a Gain K is applied to a transfer function $H(s)$ in a closed loop:
% $$ \frac{H(s)}{R(s)} = \frac{KH(s)}{1+kH(s)} $$

% if $ H(s) = \frac{b(s)}{a(s)} $
% Let n be the order of a(s) and m the order of b(s)
% closed loop poles can be calculated through:

% $$ a(s)+Kb(s)=0 $$
% or
% $$ \frac{a(s)}{K}+b(s)=0 $$

% n - m > Number of zeros at infinity (asymptotes)

% Poles near imaginary axis greater influence. 3rd order systems may look
% as first order ones.

s = tf('s');
sys = (s+7) / (s * (s+5) * (s+15) * (s+20));
rlocus(sys)
axis([-22 3 -15 15])


%% Choosing a Value of K from the Root Locus

% sgrid(zeta, wn) plots lines of constant damping ratio and natural
% frequency.

% Overshoot < 5% => zeta = 0.7
% Rise time = 1s => wn = 1.8

zeta=0.7;
wn = 1.8;
sgrid(zeta,wn)

% Inside the circle, lower natural frequency.
% Outside lines lower damping ratio.

[k,poles] = rlocfind(sys)


%% Closed-Loop Response

K = 350;
sys_cl = feedback(K*sys,1)

step(sys_cl)


%% Using Control System Designer for Root Locus Design

% Control System Designer can perform the same task
s = tf('s');
plant = (s + 7) / (s * (s + 5)*(s + 15)*(s + 20));

controlSystemDesigner(plant)
