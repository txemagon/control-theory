%% Introduction System Analysis
% From: https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=SystemAnalysis
%
% Commands:  tf , ssdata , pole , eig , step , pzmap , bode , linearSystemAnalyzer

% Bounded Input Bounded Output (BIBO) definition of stability

% Compute poles and ensure they are at tje left hand plane (LHP).
s = tf('s')
G = 1 / (s^2 + 2*s + 5)
disp('Poles')
pole (G)

% Poles of the Transfer Function are the eigenvalues of the matriz A
[A,B,C,D] = ssdata(G);
disp('Eigenvalues')
eig(A)

%% System Order

%% First Order Systems
% Final Value Theorem, DC Gain and time constant

% $$ G(s) = \frac{b}{s + a} = \frac{k_{dc}}{\tau s + 1} $$
% $ k_{dc} = \frac{b}{a}$
% Time constant $ \tau = \frac{1}{a} $, time to reach 63% of the steady
% state value

% Poles at $ s=-a $. Stable if $ a \gt 0 $ 

% Step Response

k_dc = 5;
Tc = 10;
u = 2;

s = tf('s');
G = k_dc/(Tc*s+1)

step(u*G)

linearSystemAnalyzer('step', G) % Change characteristics and peak time

% Setting time
% ------------
% Tolerances
% 10% => Ts = 2.3/a = 2.3 Tc
%  5% => Ts = 3.0 Tc
%  2% => Ts = 3.9 Tc
%  1% => Ts = 4.6 Tc

% Rise Time
%----------
% Time to rise from 10% to 90%


% Bode Plots
%-----------
bode(G)

% Magnitude is shown as $ M_{dB} = 20 log(M) $
% As of s=a Magnitude decays at 20dB/decade


%% Second-order Systems

% $$ G(s) = \frac{1}{m s^2 + b s + k} = \frac{ k_{dc} \omega_n^2}{ s^2 + 2 \zeta \omega_n s + \omega_n^2} $$

% DC Gain: 1/k
% Damping ratio:
% $$ \zeta = \frac{b}{2 \sqrt{ k \cdot m }} $$
% Natural frequency
% $ \omega_n = \sqrt{ \frac{k}{m} }$

% Poles / zeros
% The canonical 2nd order function has two poles at:
% $$ s_p = - \zeta \omega_n \plusminus j \omega_n \sqrt{1-\zeta^2} $$

% Underdamped systems $ \zeta < 1 $
% $$ \omega_d = \omega_n \sqrt{1-\zeta^2} $$

k_dc = 1;
w_n = 10;
zeta = 0.2;

s = tf('s')
G1 = k_dc * w_n^2 / (s^2 + 2*zeta*w_n*s + w_n^2);

pzmap (G1)
axis([-3 1 -15 15])

step(G1)
axis([0 3 0 2])

% Setting Time
%-------------

% $ T_s = \frac{- \ln(tolerance_fraction) }{\zeta\omega_n} $
% Setting time
% ------------
% Tolerances
% 10% => Ts = 2.3/a = 2.3 Tc
%  5% => Ts = 3.0 / (zeta*w_n)
%  2% => Ts = 3.9 / (zeta*w_n)
%  1% => Ts = 4.6 / (zeta*w_n)

% Percent Overshoot
%------------------

% $$ M_p = \exp{ \frac{-\zeta \pi}{\sqrt{1 - \zeta^2}} } $$

% Settling time             Ts   -> 4.6 / (zeta w_n)
% Rising time 10 - 90 %     Tr   -> 1.8 / w_n
% Damping Ratio             zeta -> -ln(Mp) / sqrt( pi^2 + ln²(Mp) )


%% Overdamped systems zeta > 1

zeta = 1.2;

G2 = k_dc*w_n^2/(s^2 + 2*zeta*w_n*s + w_n^2);

pzmap(G2)
axis([-20 1 -1 1])

step(G2)
axis([0 1.5 0 1.5])

%% Critically-damped systems. zeta=1
% Both poles are real sp = -zeta w_n

zeta = 1;

G3 = k_dc*w_n^2/(s^2 + 2*zeta*w_n*s + w_n^2);

pzmap(G3)
axis([-11 1 -1 1])

step(G3)
axis([0 1.5 0 1.5])


%% Undamped systems. zeta=0

zeta = 0;

G4 = k_dc*w_n^2/(s^2 + 2*zeta*w_n*s + w_n^2);

pzmap(G4)
axis([-1 1 -15 15])
step(G4)
axis([0 5 -0.5 2.5])


%% Bode Plot

bode(G1,G2,G3,G4)
legend('underdamped: zeta < 1','overdamped: zeta > 1','critically-damped: zeta = 1','undamped: zeta = 0')

% Bode plots drops at -40dB per decade for underdamped systems
% Size of the peak depends on the quality factor: Q = 1 / (2 zeta)
