t = -100:.1:0;

figure
hold on

% Vertical Lines go into concentric circles
for xv = -10:0
    s = xv + t * i;
    z = exp(s);
    plot( real(z(:)), imag(z(:)) )
end

% Horizontal lines transform themselves to a star
for xv = 0:pi/6:2*pi
    s = t  + xv * i;
    z = exp(s);
    %plot( real(z(:)), imag(z(:)) )    
end


% Diagonal lines
% Horizontal lines transform themselves to a star
t = 0:.1:100;
for alpha = pi/2:pi/12:3*pi/2
    s = t * exp(i*alpha);
    z = exp(s);
    plot( real(z(:)), imag(z(:)) )    
end

axis on
grid on

hold off