%% Digital Controller Design
% From: https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlDigital

m = 1;
b = 10;
k = 20;

s = tf('s');
sys = 1 / (m*s^2 + b*s +k);

Ts = 1/100;
sys_d = c2d(sys, Ts, 'zoh')

%% Continuous space representation of the system

A = [0    1;
    -k/m -b/m];

B = [ 0;
     1/m];
 
C = [1 0];

D = [0];

Ts = 1/100;

sys = ss(A,B,C,D);
sys_d = c2d(sys, Ts, 'zoh')

%% Stability and Transient Response
%
% Stability can be analysed in the z plane
%
% $$ z = e^{sT} $$
%
% $$ \zeta \omega_n \ge \frac{4.6}{T_s} $$
% $$ \omega_n \ge \frac{1.8}{T_r} $$
% $$ \zeta = \frac{-\ln(M_p)}{\sqrt{\pi^2 + \ln(M_p)}} $$
%
% wn in the z-plane has units of rad/sample. Equations above need rad/s
%
% Suppose:
% $$ \frac{Y(z)}{F(z))} = \frac{1}{z^2 - 0.3z + 0.5} $$

numDz = 1;
denDz = [1 -0.3 0.5];
sys = tf(numDz, denDz, -1); % The -1 indicates that the sample time is undetermined

pzmap(sys)
axis([-1 1 -1 1])
zgrid

sys = tf(numDz, denDz, 1/20);
step(sys, 2.5);

%% Discrete Root Locus
%
% Characteristic equation: $ 1 + KG(z)H_{zoh}(z) = 0 $
% G(z): Digital controller
% H_{zoh}(z): Plant transfer function in z-domain
%
% Suppose:
%
% $$ \frac{Y(z)}{F(z))} = \frac{z - 0.3}{z^2 - 1.6z + 0.7} $$
%
% Damping ratio > 0.6, wn > 0.4 rad/sample required

numDz = [1 -0.3];
denDz = [1 -1.6 0.7];
sys = tf(numDz, denDz, 1);

rlocus(sys)
axis([-1 1 -1 1])

zeta = 0.4;
Wn = 0.3;
zgrid(zeta, Wn)