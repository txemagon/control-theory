%% CONVERTING BETWEEN SYSTEM REPRESENTATIONS

% State Space - Transfer Function - Zero Pole Gain - System Variables

%%
% $$G(s) = \frac{2s + 1}{4s^2+3s+2}$$ 

s = tf('s');
G = (2*s+1)/(4*s^2+3*s+2)


% In the same manner

num = [2 1];
den = [4 3 2];
G = tf(num,den)


% Extract the state-space model from the system variable

[A,B,C,D] = ssdata(G)


% Then state-space representation can be stored in system variable H

H = ss(A,B,C,D)


% Poles, zeros and gain can be also extracted
[z,p,k]=zpkdata(H,'v')

% We store it in a system variable
K = zpk(z,p,k)

% Extract the transfer function representation
[num,den]=tfdata(K,'v')


%% State-Space to Transfer Function
disp('State-Space to Transfer Function')
[num, den] = ss2tf(A,B,C,D)

%% Zeros at Infinity
% When there are more poles than zeros, those zeros at infinity are
% computed as big numbers. Don't allow numerators with zeros at 0.00000

A = [0    1    0      0
     0 -0.1818 2.6727 0
     0    0    0      1
     0 -4.5454 31.1818 0];

B = [0
     1.8182
     0
     4.5455];

C = [1 0 0 0
    0 0 1 0];

D = [0
     0];

[num,den]=ss2tf(A,B,C,D)

fprintf ('Care with 0.0000\n')

num(1,4) = 0;
num(2,5) = 0;

disp('Better')
num
den

%% Transfer Function to State-Space

disp('Transfer Function to State-Space')
[A,B,C,D]=tf2ss(num,den)

% Backwards transformation yields the ss in control canonical form


%% State-Space and Transfer Function to zero/pole/gain:
disp('Convert to Zero Pole Gain')
[z,p,k]=tf2zp(num,den)
[z,p,k]=ss2zp(A,B,C,D)

% One column in the zeros' matrix for every row in the tf num. Watch:
num = [1.8182    0       -44.5460;
	   4.5455   -7.4373   0];

den = [1 0.1818 -31.1818 6.4786 0];

[z,p,k] = tf2zp(num,den)



%% Pole/Zero to State-Space and Transfer Function:
disp('Convert from Pole/Zero')

[A,B,C,D] = zp2ss(z,p,k)
% Conversion backwards yield an equivalent, but not identical, set of
% matrices.

[num,den] = zp2tf(z,p,k)



