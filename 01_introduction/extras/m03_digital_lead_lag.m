%% Digital lead lag compensators design

% $$ C_{lag}(z) = K_d\frac{z-z_0}{z-z_p} $$
%
% To prevent affecting the steady state response, 
% G(s=0) = G'(z=1)
% $$ K_d = \frac{1 - z_p}{1 - z_0} $$

%% Lead compensator
% The pole Zp must be a real value inside the unit circle
% Lead: z0 > zp -> Gain > 1

% Lead compensator near one of the plant's poles to get pole-zero
% cancellation.
% The root locous will shift to the left

%% Lag compensator

% zp real inside the unit circle.
% z0 < zp -> gain < 1

% If you need to increase gain to reduce steady-state error
% Put a compensator pole close to z=1
% A compensator zero to the left of the rightmost  plant pole.

% Remember: Gain goes to infinity as the closed-loop poles approach
% open-loop zeros.
% Gain goes to 0 as the closed loop poles approach open-loop poles