%% System Identification
% From: https://ctms.engin.umich.edu/CTMS/index.php?aux=Extras_Identification

% Damping ratio, natural frequency and DC gain can be found using the step
% response or Bode plot.

%% Step Response
% A non zero step input causing a zero slope output must be second order or
% higher by at least to relative degree

% A damped oscillation must be second order

%% Bode Plot

% Phase dropping below 90º => 2nd order or higher.. Relative degree as
% great as the number of -90 degreess achieved asymptotically at the
% lowest point on the phase plot of the system.

%% Identifying a System from the Step Response

% DC Gain, K, is the ratio from response to input

% Damping ratio. For an underdamped second order syste, if Maximum percent
% overshoot is known, can be calculated as:

% Damping Ratio
% $$ \zeta = \frac{-\ln(M_p)}{ \sqrt{\pi^2 + \ln^2{M_p}} } $$

% Natural Frequency
% Measure de damped natural frequency $ \omega_d $ and use the above
% damping ratio

% $$ \omega_n = \frac{\omega_d}{\sqrt{1-\zeta^2}} $$
% And $ \omega_d=\frac{2\pi}{\Delta t}$


%% Identifying a System from the Bode Plot

% DC Gain: Derived from the magnitude of the Bode plot at $ s=0 $

% $$ K = 10^{\frac{M(0)}{20}} $$

% Damping Ratio. You'll need DC gain and and Magnitude when phase is -90º

% $$ \zeta = \frac{K}{2 \cdot 10^{M (-90º) / 20}} $$

%% Identifying System Parameters

% For first order systems

% $$ G(s) = \frac{b}{(s+a)} = \frac{K}{\tau s + 1} $$

% For second order systems
% $$ G(s) = \frac{a}{s^2+bs+c} = \frac{K\omega_n^2}{s^2+2\zeta\omega_n+\omega_n^2} $$
