%% Lead-Lag Compensators

% $$ C(s) = K_c \frac{s - z_0}{s - p_0} $$
% Lead compensator z0 < P0
% A lead compensator moves the root locus to the left.

% The intersection of the asymptotes on the real axis is
% $$ a = \frac{\sum poles - \sum zeros}{\#poles - \#zeros} $$

%% Lead controller

% C(s) = (1 + aTs)/(1 + Ts)
% p = 1 / T
% z = 1/(aT)
% Kc = a

% The lead controller adds phase from 1/(aT) to 1/T
% Center frequency = 1/(T sqrt(a))
% Maximum Phase sin phi = (a-1) / (a+1)

% Increases crossover frequency
% Decreases Rise time
% Decreases the settling time
% Amplifies high frequency noise

%% Lag controller

% $$ C(s) = \frac{s - z_0}{s - p_0} $$
% Improves steady state error and doesn't affect transient response
% At High freq the gain is close to 1
% At low freq gain = z0/p0, which is > 1