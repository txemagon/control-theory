%% PID Controller Design
% From: https://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlPID
%
% Commands: tf , step , pid , feedback , pidtune

%% PID Overview
%
% $$ u(t) = K_p e(t) + K_i \int e(t) dt + K_d \frac{de}{dt} $$
%
% e(t): Tracking error.
% u(t): Output of the controllet to the plant.

% $$ K_p + \frac{K_i}{s} + K_d s = \frac{K_d s^2 + K_p s + K_i}{s} $$

Kp = 1;
Ki = 1;
Kd = 1;

s = tf('s');
C = Kp + Ki/s + Kd*s

% Alternatively

C = pid(Kp,Ki,Kd)
% And convert the pid object to a transfer function.
tf(C)

%% P,I,D, Characteristics

% Bigger Kp reacts more quickly but overshoot more. Reduces but doesn't
% eliminate steady-state error.

% Kd anticipates error. Anticipation adds damping to the system and
% decreases overshoot. Does not affect steady state error.

% Ki reduces steady state error. The system can become sluggish and 
% oscillatory.

%  CL RESPONSE  RISE TIME    OVERSHOOT   SETTLING TIME   S-S ERROR
%     Kp        Decrease     Increase    Small Change    Decrease
%     Ki        Decrease     Increase    Increase        Decrease
%     Kd        Small Change Decrease    Decrease       No Change 


%% Mass-spring-damper system

% $$ F = kx + b\dot{x} + m\ddot{x} $$
% $$ ms^2X(s)+bsX(s)+kX(s) = F(s) $$
% $$ \frac{X(s)}{F(s)} = \frac{1}{ms^2 + bs + k} $$

m = 1
b = 10
k = 20
F = 1

s = tf('s');
P = 1 / (s^2 + 10*s + 20);
step(P)

%% Proportional Control

% Closed loop function:
% $$ T(s) = \frac{X(s)}{R(s)} = \frac{K_p}{ms^2 + bs + (k + K_p)}  $$
Kp = 300;
C = pid(Kp)
T = feedback (C*P,1)

t = 0:0.01:2;
step(T,t)

%% Proportional-Derivative Control

% The derivative part will reduce the overshoot and settling time.
% $$ T(s) = \frac{X(s)}{R(s)} = \frac{K_d s + K_p}{ms^2 + (b + K_d)s + (k + K_p)} $$

Kp = 300;
Kd = 10;
C = pid(Kp,0,Kd)
T = feedback(C*P,1)

t = 0:0.01:2;
step(T,t)

%% Proportional-Integral Control

% Decreases rise time, increase both the overshoot and the settling time.
% Reduces the steady-state error.

% $$ T(s) = \frac{X(s)}{R(s)} = \frac{K_p + \frac{K_i}{s}}{ms^2 + bs + (k + K_p) + \frac{K_i}{s}} = \frac{K_p s + K_i}{ms^3 + bs^2 + (k + K_p)s + K_i} $$

Kp = 30;
Ki = 70;
C = pid(Kp,Ki)
T = feedback(C*P,1)

t = 0:0.01:2;
step(T,t)


%% Proportional-Integral-Derivative Control
%
% $$ T(s) = \frac{X(s)}{R(s)} = \frac{K_ds + K_p + \frac{K_i}{s}}{ms^2 + (b
% + K_d)s + (k + K_p) + \frac{K_i}{s}} = \frac{K_ds^2 + K_p s + K_i}{ms^3 +
% (b+K_d)s^2 + (k + K_p)s + K_i} $$

Kp = 350;
Ki = 300;
Kd = 50;
C = pid(Kp,Ki,Kd)
T = feedback(C*P,1);

t = 0:0.01:2;
step(T,t)

%% Tips for PID Design

% 1. Obtain open-loop response and determine what needs to be improved
% 2. Add a proportional to improve the rise time
% 3. Add a derivative control to reduce the overshoot
% 4. Add an integral control to reduce steady-state erro
% 5. Fine tune the system.

% Not all three strategies are always required.


%% Automatic PID tunint

% Mathlab provides the algorithm: pidtune
% Mathlab provides a GUI: pidTuner
% pidtune works for a 60º phase stability margin

% pidTuner(P,'p') % Proportional tuning
pidTuner(P,C)

% Higher bandwidth => Faster rise time
% Larger phase margin reduces overshoot and improves stability

% Using pidtune
opts = pidtuneOptions('CrossoverFrequency', 32, 'PhaseMargin', 90);
[C, info] = pidtune(P, 'pid', opts)